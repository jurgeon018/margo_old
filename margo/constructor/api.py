from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from .models import *
from utils.utils import get_sk
from django.utils import timezone
from time import time
from django.shortcuts import render
from django.shortcuts import redirect
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.files.base import ContentFile
from django.core.files import File
from PIL import Image
from io import BytesIO, StringIO
import sys


def constructor(request, **kwargs):
  sk              = get_sk(request); print('constructor sk:',sk)

  colours         = Colour.objects.all()
  form_factors    = FormFactor.objects.all()
  embossings      = Embossing.objects.all()
  foldings        = Folding.objects.all()

  default_napkin  = NapkinTemplate.objects.filter(default=True).first()
  default_url     = default_napkin.image.url
  # napkin, created = Napkin.objects.get_or_create(sk=sk) # old
  napkin = Napkin.objects.filter(sk=sk, ordered=False) # ! new
  # if created: # old
  if napkin.exists(): # !
    napkin = napkin.first() # !
    napkin.template = default_napkin
    napkin.final    = default_napkin.image
    napkin.logo     = None 
    napkin.save()
  else:
    napkin = Napkin.objects.create(sk=sk, ordered=False) # !
    default_url = napkin.final.url
    napkin.save()
  return render(request, 'constructor.html', locals())


@csrf_exempt
def set_param(request):
  print(request.POST)
  napkin          = Napkin.objects.get(sk=get_sk(request), ordered=False)

  folding_code    = request.POST.get('folding_code',     napkin.template.folding.code)
  colour_code     = request.POST.get('colour_code',      napkin.template.colour.code)
  form_factor_code= request.POST.get('form_factor_code', napkin.template.form_factor.code)
  embossing_code  = request.POST.get('embossing_code',   napkin.template.embossing.code)
  logo            = request.FILES.get('file',            napkin.logo)
  
  folding       = Folding.objects.get(code = folding_code)
  colour        = Colour.objects.get(code = colour_code)
  form_factor   = FormFactor.objects.get(code = form_factor_code)
  embossing     = Embossing.objects.get(code = embossing_code)
  
  new_template  = NapkinTemplate.objects.get(
    folding     = folding,
    colour      = colour,
    # embossing   = embossing,
    # form_factor = form_factor,
  )
  napkin.template = new_template
  napkin.logo     = logo
  napkin.final    = new_template.image
  napkin.save()
  try:
    napkin = paste_logo_on_template(napkin=napkin, request=request)
  except Exception as e:
    print('e:', e)
  napkin_url = napkin.final.url 
  return HttpResponse(napkin_url)


def paste_logo_on_template(napkin, **kwargs):
  now           = time()
  logo_path     = napkin.logo.path
  template_path = napkin.template.image.path
  
  template      = Image.open(template_path).convert('RGBA')
  template      = template.resize((1000, 1000), Image.ANTIALIAS)
  logo          = Image.open(logo_path).convert('RGBA')
  logo          = resize_logo(logo)
  template.paste(logo, (160, 200), logo)
  return create_final_image(template, napkin, request=kwargs['request'])


def resize_logo(logo, **kwargs):
  baseheight = 500
  wsize      = int((float(logo.size[0]) * float((baseheight / float(logo.size[1])))))
  logo        = logo.resize((wsize, baseheight), Image.ANTIALIAS)
  return logo


def create_final_image(template, napkin, **kwargs):
  now = time()
  now = get_sk(kwargs['request'])
  now = kwargs['request'].session.session_key
  thumb_io = BytesIO()
  template.save(thumb_io, format='PNG') # без этой сторчки все отваливается
  thumb_file = InMemoryUploadedFile(
    thumb_io, None, f'{now}.jpg', 'image/png', sys.getsizeof(thumb_io), None
  )
  napkin.final.save(f"{now}.png",thumb_file)
  napkin.save()
  return napkin