from django.contrib import admin

from .models import *

class NapkinAdmin(admin.ModelAdmin):
    # def has_add_permission(self, request, obj=None):
    #     return False

    # def has_delete_permission(self, request, obj=None):
    #     return False
        
    list_display = [ 
        'id',
        'final',
        'logo',
        'template',
        'sk',
    ]
    # can_delete = False
    readonly_fields = [
        'id',
        'final',
        'logo',
        'template',
        'sk'
    ]

admin.site.register(Napkin, NapkinAdmin)

admin.site.register(NapkinTemplate)
admin.site.register(Colour)
admin.site.register(FormFactor)
admin.site.register(Embossing)
admin.site.register(Folding)