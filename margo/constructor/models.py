from django.db import models
from PIL import Image
from django.utils.translation import gettext as _


NAPKIN_STATUS_CHOICES = (
	(_('Принят в обработку'), _('Принят в обработку')),
	(_('Выполняется'), _('Выполняется')),
	(_('Выполнен'), _('Выполнен')),
	# (_('Оплачен'), _('Оплачен')),
)


class Napkin(models.Model):
  sk       = models.CharField(max_length=120, blank=True, null=True)
  final    = models.ImageField(verbose_name=("Финальное изображение"), blank=True, null=True)
  logo     = models.ImageField(blank=True, null=True)
  template = models.ForeignKey('constructor.NapkinTemplate', verbose_name="шаблон", on_delete=models.CASCADE, blank=True, null=True)
  
  name     = models.CharField(verbose_name='Имя',max_length=120, blank=True, null=True)
  email    = models.CharField(max_length=120, blank=True, null=True)
  phone    = models.CharField(verbose_name='Номер телефона',max_length=120, blank=True, null=True)
  address  = models.CharField(verbose_name='Адрес',max_length=120, blank=True, null=True)
  comments = models.TextField(verbose_name='Коментарии',blank=True, null=True, default=None)
  # order    = models.ForeignKey('order.Order', related_name="napkins",on_delete=models.CASCADE, blank=True, null=True)
  ordered  = models.BooleanField(default=False)
  status    = models.CharField(max_length=120, blank=True, null=True, choices=NAPKIN_STATUS_CHOICES) 
  created  = models.DateTimeField(verbose_name='Дата создания',auto_now_add=True, auto_now=False, blank=True, null=True)
  updated  = models.DateTimeField(verbose_name='Дата обновления',auto_now_add=False, auto_now=True, blank=True, null=True)
  def __str__(self):
    try:
      return f'sk:{self.sk}|\nlogo:{self.logo}|\ntemplate:{self.template}'
    except Exception as e:
      return f'{self.logo}'


class NapkinTemplate(models.Model):
  image       = models.ImageField(blank=True, null=True)
  colour      = models.ForeignKey('constructor.Colour',     blank=True, null=True, on_delete=models.CASCADE)
  form_factor = models.ForeignKey('constructor.FormFactor', blank=True, null=True, on_delete=models.CASCADE)
  embossing   = models.ForeignKey('constructor.Embossing',  blank=True, null=True, on_delete=models.CASCADE)
  folding     = models.ForeignKey('constructor.Folding',    blank=True, null=True, on_delete=models.CASCADE)
  default     = models.BooleanField(default=False)
  def __str__(self): return f'{self.image}'


class Colour(models.Model):
  name = models.CharField(max_length=20, blank=True, null=True)
  code = models.CharField(max_length=20, blank=True, null=True)
  rgb  = models.CharField(max_length=20, blank=True, null=True)
  def __str__(self):
    return f'{self.name}|{self.code}|{self.rgb}'


class FormFactor(models.Model):
  name = models.CharField(max_length=20)
  code = models.CharField(max_length=20, blank=True, null=True)
  def __str__(self):
    return f'{self.name}|{self.code}'


class Embossing(models.Model):
  name = models.CharField(max_length=20)
  code = models.CharField(max_length=20, blank=True, null=True)
  image= models.ImageField(blank=True, null=True)
  def __str__(self):
    return f'{self.name}|{self.code}'


class Folding(models.Model):
  name = models.CharField(max_length=20)
  code = models.CharField(max_length=20, blank=True, null=True)
  image= models.ImageField(blank=True, null=True)
  def __str__(self):
    return f'{self.name}'