from django.db import models

class UserContact(models.Model):
  request_type = models.CharField(verbose_name='Тип запроса',max_length=120)
  email        = models.EmailField()
  phone        = models.CharField(verbose_name='Номер телефона',max_length=20)
  text         = models.TextField(verbose_name='Сообщение',blank=True, null=True)
  def __str__(self):
    try:
      return f'{self.request_type},{self.email}, {self.phone}, {self.text}'
    except:
      return f'{self.request_type},{self.email}, {self.phone}'


class Page(models.Model):
  name  = models.CharField(max_length=120, blank=True, null=True)
  title = models.CharField(max_length=120, blank=True, null=True)
  desc  = models.CharField(max_length=120, blank=True, null=True)
  url   = models.CharField(max_length=120, blank=True, null=True)
  def __str__(self):
    return f'{self.name}|{self.url}|{self.title}'




