from django.shortcuts import render
from django.urls import path, include
from django.shortcuts import render, reverse, redirect
from .models import UserContact
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect, get_object_or_404
from item.models import Item, Category
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.views.defaults import page_not_found
from django.utils import translation
from django.shortcuts import redirect
from constructor.models import NapkinTemplate, Napkin
from utils.utils import get_sk
from .models import Page

def set_language_from_url(request, user_language=None):
  # user_language = request.POST['language']
  translation.activate(user_language)
  request.session[translation.LANGUAGE_SESSION_KEY] = user_language
  # return redirect(request.META['HTTP_REFERER'])
  url = request.META['HTTP_REFERER'].split('/')
  url[3] = user_language
  url = '/'.join(url)
  return redirect(url)


def index(request):
  page = Page.objects.get_or_create(name='index')
  patterned  = Category.objects.get(code="patterned")
  monophonic = Category.objects.get(code="monophonic")
  paper      = Category.objects.get(code="paper")
  towel      = Category.objects.get(code="towel")
  towel_z    = Category.objects.get(code="towel_z")
  horeca     = Category.objects.get(code="horeca")
  return render(request, 'index.html', locals())


def horeca(request):
  MORE = True
  page = Page.objects.get_or_create(name='horeca')
  return render(request, 'horeca.html', locals())


def privatelabel(request):
  MORE = True
  page = Page.objects.get_or_create(name='privatelabel')
  return render(request, 'privatelabel.html', locals())


def partnership(request):
  MORE = True
  page = Page.objects.get_or_create(name='partnership')
  return render(request, 'partnership.html', locals())


def contact(request):
  page = Page.objects.get_or_create(name='contact')
  return render(request, 'contact.html', locals())


def search(request):
  items = Item.objects.all()
  query = request.GET.get('q')#.decode('utf-8')#.lower()
  if query:
    items = items.filter(
      Q(name__icontains=query) |
      Q(description__icontains=query)
    ).distinct()
  page = Page.objects.get_or_create(name='search')
  return render(request, 'search_results.html', locals())


def shop(request):
  categories = Category.objects.all()
  patterned  = Category.objects.get(id=1)
  monophonic = Category.objects.get(id=2)
  paper      = Category.objects.get(id=3)
  towel      = Category.objects.get(id=4)
  towel_z    = Category.objects.get(id=5)
  horeca     = Category.objects.get(id=6)
  showcase   = Item.objects.all()
  page       = Page.objects.get_or_create(name='shop')
  return render(request, 'shop.html', locals())


def servets_cat(request):
  servets_categories = Category.objects.filter(id__in=[1,2])
  return render(request, 'servets_categories.html', locals())


def category(request, slug):
  print(slug)
  print(Category.objects.all())
  category = Category.objects.get(slug=slug)
  # for cat in Category.objects.all():
  #   if cat.slug == slug:
  #     category_name = cat.title
  page = Page.objects.get_or_create(name='category')
  return render(request, 'category.html', locals())


def item_detail(request, pk):
  item = get_object_or_404(Item, pk=pk)#slug=slug)
  # items_amount = cart_item_count(request)  
  # similar = Item.objects.all()[:4]
  page = Page.objects.get_or_create(name='item')
  return render(request, 'product_card.html', locals())


def user_contact(request):
  print(request.POST)
  tel          = request.POST.get('tel','')
  email        = request.POST.get('email', '')
  text         = request.POST.get('text', '')
  request_type = request.POST.get('request_type','')
  user_order       = UserContact()
  user_order.email = email
  user_order.phone = tel
  user_order.text  = text
  user_order.request_type = request_type
  user_order.save()
  return redirect(request.META.get('HTTP_REFERER'))



def handler_404(request, exception):
  return render(request,'404.html', locals())


def handler_500(request):
  return render(request,'404.html', locals())


handler404 = 'core.views.handler_404'
handler500 = 'core.views.handler_500'

