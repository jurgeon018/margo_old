from django.contrib import admin
from .models import *

class PageAdmin(admin.ModelAdmin):
    list_display = ['id','name', 'url', 'title', 'desc']
    list_display_links = ['name']
    list_editable = ['url', 'title', 'desc']


admin.site.register(UserContact)
admin.site.register(Page, PageAdmin)


