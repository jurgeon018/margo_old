import random 
from django.core.exceptions import ObjectDoesNotExist
import string
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail


CURRENT_DOMEN = settings.CURRENT_DOMEN

def get_sk(request):
  sk = request.session.session_key
  # if not sk: print('2:', sk); request.session.cycle_key()
  return sk 


def get_user(request):
  if request.user.is_anonymous:
    return None
  return request.user


def send_comment_mail():
  send_mail(
    subject = 'Comment form Received',
    # message = get_template('contact_message.txt').render({'message':message}),
    message = f'Було отримано відгук. Перейдіть по цій ссилці: {CURRENT_DOMEN}admin/blog/comment/',
    from_email = settings.DEFAULT_FROM_EMAIL,
    recipient_list = [settings.DEFAULT_FROM_EMAIL],#, email],
    fail_silently=True,
  )


def send_contact_mail():
  send_mail(
    subject = 'Contact form Received',
    # message = get_template('contact_message.txt').render({'message':message}),
    message = f'Було отримано запитання. Перейдіть по цій ссилці: {CURRENT_DOMEN}admin/blog/contact/',
    from_email = settings.DEFAULT_FROM_EMAIL,
    recipient_list = [settings.DEFAULT_FROM_EMAIL],#, email],
    fail_silently=True,
  )


def send_order_mail():
  send_mail(
    subject = 'Order form Received',
    # message = get_template('contact_message.txt').render({'message':message}),
    message = f'Було отримано замовлення. Перейдіть по цій ссилці: {CURRENT_DOMEN}admin/order/order/',
    from_email = settings.DEFAULT_FROM_EMAIL,
    recipient_list = [settings.DEFAULT_FROM_EMAIL],#, email],
    fail_silently=True,
  )