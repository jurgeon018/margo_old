from core.models import Page
try:
    index_url        = Page.objects.filter(name='index')[0].url
    horeca_url       = Page.objects.filter(name='horeca')[0].url
    privatelabel_url = Page.objects.filter(name='privatelabel')[0].url
    constructor_url  = Page.objects.filter(name='constructor')[0].url
    partnership_url  = Page.objects.filter(name='partnership')[0].url
    contact_url      = Page.objects.filter(name='contact')[0].url
    shop_url         = Page.objects.filter(name='shop')[0].url
    servets_cat_url  = Page.objects.filter(name='servets_cat')[0].url
    category_url     = Page.objects.filter(name='category')[0].url
    item_url         = Page.objects.filter(name='item')[0].url
    order_items_url   = Page.objects.filter(name='order_items')[0].url
    order_napkin_url = Page.objects.filter(name='order_napkin')[0].url
    pay_url          = Page.objects.filter(name='pay')[0].url
    search_url       = Page.objects.filter(name='search')[0].url
    profile_url      = Page.objects.filter(name='profile')[0].url
except:
    index_url        = ""
    horeca_url       = "horeca/"
    privatelabel_url = "privatelabel/"
    constructor_url  = "constructor/"
    partnership_url  = "partnership/"
    contact_url      = "contact/"
    shop_url         = "shop/"
    servets_cat_url  = 'servets_categories/'
    category_url     = "category/"
    item_url         = "item/"
    order_items_url  = "order_items/"
    order_napkin_url = "order_napkin/"
    pay_url          = "pay/"
    thank_you_url    = "thank_you/"
    search_url       = "search/"
    profile_url      = "profile/"
