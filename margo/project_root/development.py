from .base import *
DEBUG = True
ALLOWED_HOSTS = ['*']
# ALLOWED_HOSTS = ['127.0.0.1', 'localhost']
# ALLOWED_HOSTS = ['ip-address', 'www.your-website.com']
# ALLOWED_HOSTS = ['margo-shop.herokuapp.com', 'localhost','127.0.0.1']
# INSTALLED_APPS += [
#     'debug_toolbar',
# ]
# MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware',]
# # DEBUG TOOLBAR SETTINGS
# DEBUG_TOOLBAR_PANELS = [
#     'debug_toolbar.panels.versions.VersionsPanel',
#     'debug_toolbar.panels.timer.TimerPanel',
#     'debug_toolbar.panels.settings.SettingsPanel',
#     'debug_toolbar.panels.headers.HeadersPanel',
#     'debug_toolbar.panels.request.RequestPanel',
#     'debug_toolbar.panels.sql.SQLPanel',
#     'debug_toolbar.panels.staticfiles.StaticFilesPanel',
#     'debug_toolbar.panels.templates.TemplatesPanel',
#     'debug_toolbar.panels.cache.CachePanel',
#     'debug_toolbar.panels.signals.SignalsPanel',
#     'debug_toolbar.panels.logging.LoggingPanel',
#     'debug_toolbar.panels.redirects.RedirectsPanel',
# ]
# def show_toolbar(request):
#     return True
# DEBUG_TOOLBAR_CONFIG = {
#     'INTERCEPT_REDIRECTS': False,
#     'SHOW_TOOLBAR_CALLBACK': show_toolbar
# }
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


LIQPAY_PUBLIC_KEY = "sandbox_i42240133771"
LIQPAY_PRIVATE_KEY = "sandbox_680jrXgIMcW0tpYE9vsskTHgm4hykvfLXS52Pfxx"
