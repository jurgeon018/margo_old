import os
from django.core.wsgi import get_wsgi_application
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project_root.development')
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project_root.production')
application = get_wsgi_application()