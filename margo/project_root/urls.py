from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

from core.views import *
from order.views import *
from payment.views import *
from user.views import *
from constructor.api import constructor


urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('set_language/<user_language>/', set_language_from_url, name="set_language_from_url"),
    path('', include('cart.urls')),
    path('', include('item.urls')),
    path('', include('constructor.urls')),
    path('', include('payment.urls')),
    path('', include('user.urls')),
    path('', include('core.urls')),

    # path('pages/', include('django.contrib.flatpages.urls')),
] 


# if settings.DEBUG == True:
# import debug_toolbar
# urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) 
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from .pages import *

urlpatterns += i18n_patterns(
    path('admin/',                admin.site.urls),
    path('accounts/',             include('allauth.urls')),
    path(f'{profile_url}',        profile,     name='profile'),
    path(f'{index_url}',          index,       name='index'),
    path(f'{horeca_url}',         horeca,      name='horeca'),
    path(f'{privatelabel_url}',   privatelabel,name='privatelabel'),
    path(f'{constructor_url}',    constructor, name='constructor'),
    path(f'{partnership_url}',    partnership, name='partnership'),
    path(f'{contact_url}',        contact,     name='contact'),
    path(f'{shop_url}',           shop,        name='shop'),
    path(f'{servets_cat_url}',    servets_cat, name='servets_cat'),
    path(f'{category_url}<slug>/',category,    name='category'),
    path(f'{item_url}<pk>/',      item_detail, name='item_detail'),
    path(f'{order_items_url}',    order_items, name='order_items'),
    path(f'{order_napkin_url}',   order_napkin,name='order_napkin'),
    path(f'{pay_url}',            pay,         name='pay'),
    path(f'{thank_you_url}',      thank_you,   name='thank_you'),
    path(f'{search_url}',         search,      name='search'),
    prefix_default_language=True,
)

