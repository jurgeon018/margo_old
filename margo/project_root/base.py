import os
from decouple import config
from django.utils.translation import ugettext_lazy as _
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CURRENT_DOMEN = config('DOMEN')
SECRET_KEY = config('SECRET_KEY')
ADMINS = (('Alexey', 'jurgeon018@gmail.com'), ('Andrew', 'jurgeon019@gmail.com'))
ROOT_URLCONF = 'project_root.urls'
WSGI_APPLICATION = 'project_root.wsgi.application'
LANGUAGE_CODE = 'ru-ru'
TIME_ZONE = 'UTC' #'Europe/Kiev'
USE_L10N = True
USE_I18N = True
USE_TZ = True
LANGUAGES = (
  ('ru', _('Russian')),
  ('en', _('English')),
  ('uk', _('Ukrainian')),
)
LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale'),)
STATIC_URL = '/static/'
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
STATIC_ROOT = os.path.join(BASE_DIR, "static_root")
MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = '/media/'
# LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = 'profile'
CRISPY_TEMPLATE_PACK = 'bootstrap4'
SITE_ID=1
AUTH_USER_MODEL = 'user.User'
from .includes.ALLAUTH import *
from .includes.EMAIL import *
from .includes.INSTALLED_APPS import *
from .includes.MIDDLEWARE import *
from .includes.TEMPLATES import *