from django.contrib import admin
from order.models import Order

# from django.contrib.auth.models import User
# admin.site.unregister(User)


from .models import User


class OrderInline(admin.TabularInline):
    def has_add_permission(self, request, obj=None):
        return False
    def has_delete_permission(self, request, obj=None):
        return False
    model = Order
    extra = 0
    # fields = []
    exclude = []
    can_delete = False
    list_display_links = ['id', '']
    # list_editable = []
    readonly_fields = [field.name for field in Order._meta.fields]



class UserAdmin(admin.ModelAdmin):
    class Meta:
        model = User
    inlines = [
        OrderInline,
    ]
    fields = [
        'username',
        'phone_number', 
        'first_name', 
        'last_name', 
        'email'
    ]
    list_display = [
        'username',
        'phone_number', 
        'first_name', 
        'last_name', 
        'email'
    ]
    readonly_fields = [
        # 'username',
        'phone_number', 
        'first_name', 
        'last_name', 
        'email'
    ]


admin.site.register(User, UserAdmin)

