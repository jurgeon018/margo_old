from django.urls import path
from .views import *


urlpatterns = [
    path('delete_order/<pk>/',  delete_order, name='delete_order'),
]
