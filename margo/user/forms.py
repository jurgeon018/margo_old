# from .models import Profile
from django.contrib.auth import get_user_model
from django import forms
User = get_user_model()
from django.utils.translation import ugettext as _

# class ProfileForm(forms.ModelForm):
#   class Meta:
#     model = Profile
#     # fields = ['phone']
#     exclude = []


class UserForm(forms.ModelForm):
  email      = forms.EmailField(required=False, widget=forms.TextInput(attrs={  
    'class':'form-control',
    'placeholder':'E-mail',
  }))
  # username   = forms.CharField(max_length=120, widget=forms.TextInput(attrs={  
  #   'class':'form-control',
  #   'placeholder':_('Username'),
  # }))
  first_name = forms.CharField(required=False, max_length=120, widget=forms.TextInput(attrs={  
    'class':'form-control',
    'placeholder':_('First Name'),
  }))
  last_name  = forms.CharField(required=False, max_length=120, widget=forms.TextInput(attrs={  
    'class':'form-control',
    'placeholder':_('Last Name'),
  }))
  password   = forms.CharField(required=False, max_length=120, widget=forms.TextInput(attrs={  
    'class':'form-control',
    'placeholder':_('Password'),
  }))
  class Meta:
    model = User
    fields = [
      # 'username', 
      'email', 'first_name', 'last_name', 'phone_number'
    ]
