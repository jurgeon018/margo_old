from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
  phone_number = models.CharField(max_length=120, blank=True, null=True)


# from django.contrib.auth import get_user_model
# from django.utils.translation import gettext as _

# User = get_user_model()

# class Profile(models.Model):
#   user = models.OneToOneField(User, on_delete=models.CASCADE)
#   phone_number = models.CharField(max_length=120)
#   email = models.CharField(max_length=120, blank=True, null=True)
#   def save(self, *args, **kwargs):
#     self.email = self.user.email
#     super().save(*args, **kwargs)
#   def __str__(self):
#     return f'{self.user.username}, {self.phone_number}'
#   class Meta:
#     verbose_name=_("Профіль"); verbose_name_plural=_("Профілі"); 

