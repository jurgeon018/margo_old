from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from utils.utils import get_user, get_sk
from order.models import Order
from django.contrib import messages
from .forms import *
from django.urls import reverse

@login_required
def profile(request):
    orders = Order.objects.filter(
      user=get_user(request)
    ).order_by('-id')
    # p_form = ProfileForm(
    #   request.POST or None, 
    #   request.FILES or None, 
    #   instance = request.user.profile
    # )
    # u_form = UserForm(
    #   data = request.POST or None, 
    #   # files = request.FILES or None,
    #   # instance = request.user
    # )
    print(request.scheme)
    if request.method == 'POST':# and u_form.is_valid():# and p_form.is_valid():
        first_name = request.POST.get('first_name', '')
        last_name  = request.POST.get('last_name', '')
        email      = request.POST.get('email', '')
        phone      = request.POST.get('phone_number', '')
        user, _    = User.objects.get_or_create(username=request.user.username)
        user.first_name   = first_name
        user.last_name    = last_name
        user.email        = email
        user.phone_number = phone
        user.save()
        messages.success(request, 'Ваши данные были обновлены')
        return redirect(reverse('profile'))
    return render(request, 'profile.html', locals())


@login_required
def delete_order(request, pk):
  order = get_object_or_404(Order, pk=pk, user=get_user(request))
  order.delete()
  messages.success(request, 'order was deleted')
  return redirect('profile')