var params = {};

function show_more(e){
    params['show_more_flag'] = true
    params['link'] = e.target.dataset.href
    initialize(params)
}


document.addEventListener('DOMContentLoaded', function(){
  params['slices'] = {'start':0,'end':4}
  initialize(params);
  $('.show_more').click(show_more)
})