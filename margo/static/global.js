var SITE_NAME = window.location.origin+'/'

// cart
function get_cart_items_amount(){
  $.ajax({url:SITE_NAME+"get_cart_items_amount/",async:true, type:'GET', success:function(data){
    document.querySelector('.items_counter').innerHTML = `
      <span>${data}</span>
    `
  }})
}

function get_cart_items(){
  var data = {}
  $.ajax({
    url:SITE_NAME+"get_cart_items/",
    data:data, async:true, type:'GET',
    success: function(data){
    var result = '';
    items = data['cart_items']
    items.forEach(function(cart_item){
      result += `
      <div class="product cart_product" data-id="${cart_item.id}">
        <div class="product__group">
            <div class="product__img">
                <img src="${cart_item.item.image[0].image}" alt="${cart_item.item.image[0].alt}">
            </div>
            <div class="product__content">
                <a href="${SITE_NAME}item/${cart_item.item.id}/">
                <div class="product__content_title">${cart_item.item.name}</div>
                </a>
                <div class="product__content_servise"> Код товару:
                    <div class="product__content_namber">${cart_item.item.articul}</div>
                </div>
            </div>
        </div>
        <div class="product__group">
        <div class="product__quantity" data-prise="${cart_item.price}"   >
            <div class="product__group_plus plus-btn" data-id=${cart_item.id}>+</div>
            <input class="product__quantity_count" type="text" data-price_per_item=${cart_item.price_per_item}  data-id="${cart_item.id}" name="name" value="${cart_item.quantity}">
            <div class="product__group_minus minus-btn" data-id=${cart_item.id}>-</div>
        </div>
            <div class="product__price_box">
                <div class="product__price_title">Ціна:</div>
                <div class="product__price cart_item_price" data-id="${cart_item.id}">${cart_item.total_price}</div>
                <div class="product__cur">грн</div>
            </div>
        </div>
        <div data-id="${cart_item.id}" class="product_remove cross cart_item_cross"></div>
      </div>
      `;
    })
    try{
      document.querySelector('.basket_form').innerHTML = result;
    }catch(error){
      // document.querySelector('basket__box').innerHTML = result;
    }
    pluses  = document.querySelectorAll('.plus-btn')
    minuses = document.querySelectorAll('.minus-btn')
    inputs  = document.querySelectorAll('.product__quantity_count')
    pluses.forEach(function(plus){
      plus.addEventListener('click', change_cart_item_amount)
    })
    minuses.forEach(function(minus){
      minus.addEventListener('click', change_cart_item_amount)
    })
    inputs.forEach(function(input){
      input.addEventListener('change', change_cart_item_amount)
    })

    // сирий жс
    crosses = document.querySelectorAll('.cart_item_cross')
    crosses.forEach(function(cross){
      cross.addEventListener('click', delete_cart_item)
    })
    //
    // жквері
    $('.cart_item_cross').click(delete_cart_item)
    //

  }})
}

function delete_cart_item(e){
  id = e.target.dataset.id
  data = {}
  data['id'] = id
  document.querySelectorAll('.cart_product').forEach(function(product){
    if(product.dataset.id == id){
      product.style.display = 'none'
    }
  })
  $.ajax({url:SITE_NAME+'remove_cart_item/', type:'POST', data:data, async:true, success:function(data){
    get_cart_items_amount();
  }})
}

function add_cart_item(e){
  var data = {};
  data['id'] = e.target.dataset.id;
  var count = parseInt(document.querySelector('.items_counter').innerText)+ 1
  document.querySelector('.items_counter').innerHTML = `<span>${count}</span>`
  $.ajax({url:SITE_NAME+'add_cart_item/', type:'POST', data:data, async:true, success: function(data){
    get_cart_items_amount();
    get_cart_items();
  }})
}

function change_cart_item_amount(e){

  if(e.target.innerHTML == '+'){
    var input = e.target.nextElementSibling;
    var quantity = parseInt(input.value) + 1;
  }
  else if(e.target.innerHTML == '-'){
    var input = e.target.previousElementSibling;
    var quantity = parseInt(input.value) - 1;
  }
  else{
    var input = e.target;
    var quantity = parseInt(input.value)
  }
  id = input.dataset.id;
  input.value = quantity;
  // total_order_price = document.querySelector('.не_існує')
  // total_order_price.innerHTML = data.total_order_price
  totals = document.querySelectorAll('.cart_item_price');
  totals.forEach(function(total){
    if(total.dataset.id == id){
      result = parseFloat(parseFloat(input.dataset.price_per_item).toFixed(2) * parseFloat(quantity).toFixed(2)).toFixed(2)
      total.innerHTML = result;
    }
  })
  var data = {};
  data['quantity'] = quantity;
  data['id'] = id;
  $.ajax({url:SITE_NAME+"change_cart_item_amount/", data:data, type:'POST', async:true, success:function(data){
    get_cart_items_amount();
  }})
}


// favours
function get_favours_amount(){
  $.ajax({url:SITE_NAME+"get_favours_amount/",async:true, type:'GET', success:function(data){
    document.querySelector('.likes_counter').innerHTML = `
      <span>${data}</span>
    `
  }})
}

function get_favours(){
  $.ajax({url:SITE_NAME+"get_favours/", async:true, type:'GET', success:function(data){
    var result = '';
    data['favours'].forEach(function(favour_item){
      favour_item_template = `
      <div class="product favour_product" data-item_id=${favour_item.item.id} data-id=${favour_item.id}>
        <div class="product__group">
          <div class="product__img">
            <img src="${favour_item.item.image[0].image}" alt="${favour_item.item.image[0].alt}">
          </div>
          <div class="product__content">
            <div class="product__content_title">${favour_item.item.name}</div>
            <div class="product__content_servise"> Код товару:
              <div class="product__content_namber">${favour_item.item.articul}</div>
            </div>
          </div>
        </div>
        <div class="product__group">
          <div class="product__price_box">
            <div class="product__price_title">Ціна:</div>
            <div class="product__price">${favour_item.item.price} грн</div>
          </div>
          <div class="btn product__btn add_favour_to_cart" data-favour_id=${favour_item.id} data-id="${favour_item.item.id}">Додати до кошика</div>
        </div>
        <div class="product_remove cross favour_cross" data-id="${favour_item.id}"></div>
      </div>
      `
      result += favour_item_template
    })
    document.querySelector('.favour_items_box').innerHTML = result;
    crosses = document.querySelectorAll('.favour_cross')
    add_favour_buttons = document.querySelectorAll('.add_favour_to_cart')
    add_all_favours_button = document.querySelector('.add_all_favours_button')
    crosses.forEach(function(cross){
      cross.addEventListener('click', remove_favour)
    })
    add_favour_buttons.forEach(function(button){
      button.addEventListener('click', add_favour_to_cart)
    })
    add_all_favours_button.addEventListener('click', add_favours_to_cart)
  }})
}

function add_favour(e){

  var data = {};
  data['item_id'] = e.target.dataset.id;
  $.ajax({url:SITE_NAME+'add_favour/', type:'POST', data:data, async:true, success:function(data){
    get_favours_amount();
    get_favours()
  }})
}

function remove_favour(e){
  id = e.target.dataset.id;
  var data = {}
  data['id'] = id
  document.querySelectorAll('.favour_product').forEach(function(product){
    if(product.dataset.id == id){
      product.style.display = 'none'
    }
  })
  $.ajax({url:SITE_NAME+'remove_favour/', data:data, type:'POST', async:true, success:function(item_id){
    get_favours_amount();
    document.querySelectorAll('.like__box-active').forEach(function(item){
      if(item.dataset.id === item_id)
      item.classList.remove('like__box-active')
    })
  }})
}

function handle_like(e){
  var like = this.childNodes[1]
  class_ = like.classList[like.classList.length - 1]
  var url = SITE_NAME;
  if(class_ === 'like__box-active'){
    url += 'add_favour_by_like/'
  }else{
    url += 'remove_favour_by_like/'
  }
  var data = {};
  data['item_id'] = e.target.dataset.id;
  $.ajax({url:url, data:data, async:true, method:'POST', type:'POST', success:function(response){
    get_favours();
    get_favours_amount();
  }})
}

function add_favour_to_cart(e){
  var id = e.target.dataset.id;
  var favour_id = e.target.dataset.favour_id
  var data = {}
  data['id'] = id
  data['favour_id'] = favour_id
  document.querySelectorAll('.favour_product').forEach(function(product){
    if(product.dataset.item_id == id){
      product.style.display = 'none'
    }
  })
  $.ajax({url:SITE_NAME+'add_favour_to_cart/', data:data, type:'POST', async:true, success:function(data){
    get_favours_amount();
    get_cart_items_amount();
    get_cart_items();
  }})
}

function add_favours_to_cart(e){
  var data = {}
  document.querySelectorAll('.favour_product').forEach(function(product){
    product.style.display = 'none'
  })
  $.ajax({url:`${SITE_NAME}add_favours_to_cart/`, data:data, method:'POST', type:'POST', async:true, success:function(data){
    get_cart_items_amount();
    get_cart_items()
    get_favours();
    get_favours_amount();
  }})
}

function paste_element(item, data){
  var active = '';
  if(data.items_in_favours.includes(item.id)){
    active = 'like__box-active';
  }
  var item_card = `
  <div class="catalog__item card">
    <div class="card__img">
        <a href="${SITE_NAME}item/${item.id}/">
          <img class="lazyload" src="/static/img/svg/loader.svg" data-src="${item.image[0].image}" alt="${item.image[0].alt}">
        </a>
        <div class="card__img_discont">new</div>
    </div>
    <div class="card__main">
        <div class="card__main_title">
          <a href="${SITE_NAME}item/${item.id}/">${item.name}</a></div>
        <div class="card__service">
            <div class="card__wrap-prise">
                <div class="card__discont">${item.price}</div>
                <div class="card__prise">${item.discount_price} грн</div>
            </div>
            <div class="card__like" data-id="${item.id}">
                <div class="like__box ${active}" data-id="${item.id}">
                  <svg data-id="${item.id}" width="20" height="19" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
                    <g>
                    <path data-id="${item.id}" id="svg_1" fill="#EC3A34" d="m14.5413,0.66423c-1.4604,0 -2.8336,0.57102 -3.8688,1.61147c-0.2219,0.22304 -0.4438,0.44608 -0.6657,0.66912l-0.67622,-0.67963c-1.03518,-1.04045 -2.41194,-1.61498 -3.87234,-1.61498c-1.45692,0 -2.82671,0.57103 -3.8584,1.60798c-1.03518,1.04045 -1.60331,2.42072 -1.59982,3.88506c0,1.46434 0.57161,2.8411 1.60679,3.88501l8.08626,8.1275c0.08016,0.0806 0.18821,0.1261 0.29975,0.1261c0.11158,0 0.21958,-0.0455 0.29978,-0.1226l8.1036,-8.1169c1.0352,-1.04049 1.6034,-2.42076 1.6034,-3.8886c0.0034,-1.46785 -0.5612,-2.85162 -1.5964,-3.88857c-1.0317,-1.02994 -2.4015,-1.60096 -3.8619,-1.60096zm3.2589,8.77904l-7.80738,7.81569l-7.79,-7.8297c-0.87485,-0.87931 -1.35584,-2.04588 -1.35584,-3.28251c0,-1.24364 0.48099,-2.41021 1.35236,-3.279c0.87136,-0.87581 2.02853,-1.35575 3.2589,-1.35575c1.23734,0 2.39799,0.48345 3.27284,1.36275l0.97244,0.9774c0.08017,0.08057 0.18822,0.12611 0.29978,0.12611c0.1115,0 0.2196,-0.04554 0.2997,-0.12611l0.9655,-0.97039c0.8748,-0.87931 2.0355,-1.36275 3.2659,-1.36275c1.2303,0 2.3875,0.48344 3.2623,1.35574c0.8714,0.8758 1.3524,2.04237 1.3524,3.28601c0.0035,1.23313 -0.474,2.3997 -1.3489,3.28251z"/>
                    <path data-id="${item.id}" id="svg_2" d="m10.028206,3.842239c3.48771,-7.45426 20.19914,0 0,13.85584c-20.19914,-13.85584 -3.48771,-21.87323 0,-13.85584z" stroke-linecap="null" stroke-linejoin="null" stroke-dasharray="null" stroke-width="null" fill="#ec3a34"/>
                    </g>
                  </svg>
                </div>
            </div>
        </div>
        <div class="card__button" data-id=${item.id}>До кошика</div>
      </div>
  </div>
  `
  return item_card
}

function handle_pagination(data){
  if(!data['has_prev']){
    $('.prev').hide();
  }else{
    $('.prev').show();
  }
  if(!data['has_next']){
    $('.next').hide();
  }else{
    $('.next').show();
  }
  $('.prev').attr('data-href',  data['prev_url'])
  $('.next').attr('data-href',  data['next_url'])
  var page_range = '';
  data.page_range.forEach( (page_number) => {
    if(page_number === data.current_page){
      page_range += `
      <div data-href="?page=${page_number}" class="link catalog__pagination_item catalog__pagination_item-active">${page_number}</div>
      `
    }else{
      page_range += `
      <div data-href="?page=${page_number}" class="link catalog__pagination_item">${page_number}</div>
      `
    }


  })
  $('.page_range').html(page_range)
  // if(!($('.link') == null)){
  //   $('.link').click(select_page)
  // }
  try{
    // $('.link').on('click', select_page)
    document.querySelectorAll('.link').forEach(function(link){
      link.addEventListener('click', select_page)
    })
  }catch{}
}

function handle_more(data){
  if(!data['has_next']){
    $('.show_more').hide();
  }
  $('.show_more').attr('data-href', data['next_url'])
  if($('.show_more') === true){
    $('.show_more').click(show_more)
  }
}

class UI {
  displayProducts(data){
    var items;
    // items = data.items;
    items = data.paginated_items;

    var result = '';
    items.forEach(function(item){
      result += paste_element(item, data)
    })
    if(data['params']['show_more_flag'] === true){
      try{
        document.querySelector('.catalog__wrap').innerHTML += result;
      }catch{
        document.querySelector('.product_comra').innerHTML += result;
      }
    }else{
      try{
        document.querySelector('.catalog__wrap').innerHTML = result;
      }catch{
        document.querySelector('.product_comra').innerHTML = result;
      }
    }
    handle_pagination(data)
    handle_more(data)
  }

  cartLogic(){
    $('.card__button').on('click', add_cart_item)
    // $('.card__like').on('click', add_favour)
    $('.card__like').on('click', handle_like)
  }

  observe(){
    var imageObserver = new IntersectionObserver(function (entries, imgObserver) {
      entries.forEach(function (entry) {
        if (entry.isIntersecting) {
          var lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.classList.remove("lzy_img");
          imgObserver.unobserve(lazyImage);
        }
      });
    });
    var arr = document.querySelectorAll('.lazyload');
    arr.forEach(function (v) {
      imageObserver.observe(v);
    });
  }

  toggle_like(){
    $(".like__box").on('click', function(e) {
      $(this).toggleClass('like__box-active');
    });
  }

}


class Products {
  async getProducts(params) {
    try {
      var url = '';
      url = SITE_NAME+"get_items/";

      try{
      var category = window.location.href.split('category')[1].replace('/','');
      }catch{}
      if(category){
        url += category;
      }

      if($.isEmptyObject(params['args'])){
      }else{
        var size    = params['args']['size']
        var layers  = params['args']['layers']
        var sorting  = params['args']['sorting']
        url += `?size=${size}&layers=${layers}&sorting=${sorting}`;
      }
      if($.isEmptyObject(params['link'])){
      }else{
        var link = params['link']
        url += `?${link}`
      }
      var result = await fetch(url)
      var data   = await result.json();
      var products;
      if($.isEmptyObject(params['slices'])){
        products = data.items;
      }else{
        var start = params['slices']['start']
        var end   = params['slices']['end']
        products = data.items.slice(start,end);
      }
      data['items'] = products;
      data['params'] = params
      return data;
    } catch (error) {
      console.log(error)
    }
  }
}

// forEach method, could be shipped as part of an Object Literal/Module start
var forEach = function(array, callback, scope) {
  for (var i = 0; i < array.length; i++) {
    callback.call(scope, i, array[i]); // passes back stuff we need
  }
};
function hover_card_item() {
  var card_item = document.getElementsByClassName('catalog__item');
  forEach(card_item, function(index, value) {
    value.onmouseover = function(event) {
      value.classList.add('catalog__item-active');
    };


    value.onmouseout = function(event) {
        value.classList.remove('catalog__item-active');
    };

  })
}


function initialize(params){
  const ui = new UI();
  const products = new Products();
  products
    .getProducts(params)
    .then(data => {
      $('.loading').show()
      ui.displayProducts(data);
    })
    .then(() => {
      ui.cartLogic();
      ui.observe();
      $('.loading').hide()
      hover_card_item();
      ui.toggle_like();
    });
}


document.addEventListener('DOMContentLoaded', function(){
  get_cart_items_amount();
  get_favours_amount();
  get_cart_items();
  get_favours();
})