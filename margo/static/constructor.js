function handle_click(e){
  var data = {}
  try{
  data['colour_code']      = e.target.dataset.colour_code
  }catch{}
  try{
  data['form_factor_code'] = e.target.dataset.form_factor_code
  }catch{}
  try{
  data['embossing_code']   = e.target.dataset.embossing_code
  }catch{}
  try{
  data['folding_code']     = e.target.dataset.folding_code
  }catch{}
  console.log(e.target)
  $.ajax({
    url:SITE_NAME+"set_param/",
    data:data,
    async:true,
    method:'POST',
    type:'POST',
    success:function(data){
      console.log(data)
      $('.constructor__result').attr('src', data)
    }
  })
}
function handle_photo(e){
  files = e.target.files
  var formData = new FormData();
  for(var i=0; i<files.length; i++){
    formData.append('file', files[i])
  }
  // for (var p of formData) {
  //   console.log(p);
  // }
  $.ajax({
    url:SITE_NAME+"set_param/",
    data:formData,
    async:true,
    contentType: false,
    processData: false,
    method:'POST',
    type:'POST',
    success:function(data){
      $('.constructor__result').attr('src', data)
    }
  })
}
document.addEventListener('DOMContentLoaded', function(){
  $('.colouritem').click(handle_click)
  $('.formfactoritem').click(handle_click)
  $('.embossingitem').click(handle_click)
  $('.foldingitem').click(handle_click)
  $('#photo').change(handle_photo)
})