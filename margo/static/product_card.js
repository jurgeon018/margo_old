document.addEventListener('DOMContentLoaded', function(){
  var productcart = document.querySelector('#product-cart')
  // var size = document.querySelector('.select_value_name')
  var product__quantity_count = document.querySelector('.product__quantity_count')
  productcart.addEventListener('submit', function(e){
    e.preventDefault();
    var id = productcart.dataset.id
    var data = {};
    data['id'] = id
    // data['size'] = size.innerHTML
    data['quantity'] = product__quantity_count.value
    $.ajax({url:SITE_NAME+"add_cart_item/", data:data, type:'POST', async:true, success:function(data){
      get_cart_items_amount()
      product__quantity_count.value = 1
      get_cart_items()
    }})
  })
})