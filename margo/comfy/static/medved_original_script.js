var form = $('#form_buying_product');
var submit_btn = $('#submit_btn')

function basketUpdating(product_id, nmb, is_delete) {
  var data = {};
  data.product_id = product_id;
  data.nmb = nmb;
  data["csrfmiddlewaretoken"] = $('#form_buying_product [name="csrfmiddlewaretoken"]').val();
  data["is_delete"] = is_delete
  $.ajax({
    url: form.attr("action"),
    type: 'POST',
    data: data,
    cache: true,
    success: function (data) {
      console.log(data.products_total_nmb)
      if (data.products_total_nmb || data.products_total_nmb == 0) {
        $('#basket_total_nmb').text("(" + data.products_total_nmb + ")");
        $('.basket-items ul').html("");
        $.each(data.products, function (k, v) {
          $('.basket-items ul').append('<li>' + v.name + ', ' + v.nmb + 'шт. ' + 'по ' + v.price_per_item + 'грн  ' +
            '<a class="delete-item" href="" data-product_id="' + v.id + '">x</a>' +
            '</li>');
        });
      }
    },
    error: function () {
      console.log("error")
    }
  })
}

function calculatingBasketAmount() {
  var total_order_amount = 0;
  $('.total-product-in-basket-amount').each(function () {
    total_order_amount = total_order_amount + parseFloat($(this).text());
  });
  $('#total_order_amount').text(total_order_amount.toFixed(2));
};

$(document).ready(function () {
  
  // DOMContentLoader
  calculatingBasketAmount();

  // product.html
  submit_btn.on('click', function (e) {
  // form.on('submit', function (e) {
    e.preventDefault();
    var nmb = $('#nmb').val();
    var product_id = submit_btn.data("product_id");
    var is_delete = false;
    basketUpdating(product_id, nmb, is_delete)
  });

  // navbar.html
  $('.basket-container').mouseover(function () {
    $('.basket-items').removeClass('hidden');
  });
  $('.basket-container').mouseout(function () {
    $('.basket-items').addClass('hidden');
  });
  $(document).on('click', '.delete-item', function (e) {
    e.preventDefault();
    var product_id = $(this).data("product_id")
    var nmb = 0;
    var is_delete = true;
    basketUpdating(product_id, nmb, is_delete)
  });

  // checkout.html
  $(document).on('change', ".product-in-basket-nmb", function () {
    var current_nmb = $(this).val();
    var current_tr = $(this).closest('tr');
    var current_price = parseFloat(current_tr.find('.product-price').text()).toFixed(2);
    var total_amount = parseFloat(current_nmb * current_price).toFixed(2);
    current_tr.find('.total-product-in-basket-amount').text(total_amount);
    calculatingBasketAmount();
  });


});