

def comfy(request):
    return render(request, 'comfy.html', locals())


urlpatterns = [
    path('comfy/', comfy, name='comfy'),
]