from rest_framework import serializers, validators, renderers, generics
from django.http import JsonResponse, HttpResponse
from .models import *
from django.shortcuts import render
from cart.models import CartItem
from utils.utils import get_user, get_sk
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.csrf import csrf_exempt


def comfy_api(request):
  response = {
      "items": [
        {
            "id": "1",
            "title": "queen panel bed",
            "price": 10.99,
            "image":"/media/product-1.jpeg"
        },
        {
            "id": "2",
            "title": "king panel bed",
            "price": 12.99,
            "image":"/media/product-2.jpeg"
        },
        {
            "id": "3",
            "title": "single panel bed",
            "price": 12.99,
            "image":"/media/product-3.jpeg"
        },
        {
            "id": "4",
            "title": "twin panel bed",
            "price": 22.99,
            "image":"/media/product-4.jpeg"
        },
        {
            "id": "5",
            "title": "fridge",
            "price": 88.99,
            "image":"/media/product-5.jpeg"
        },
        {
            "id": "6",
            "title": "dresser",
            "price": 32.99,
            "image":"/media/product-6.jpeg"
        },
        {
            "id": "7",
            "title": "couch",
            "price": 45.99,
            "image":"/media/product-7.jpeg"
        },
        {
            "id": "8",
            "title": "table",
            "price": 33.99,
            "image":"/media/product-8.jpeg"
        }
      ]
    }
  return JsonResponse(response)


