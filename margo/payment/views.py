from django.shortcuts import render, redirect
from order.models import Order
from django.views.decorators.csrf import csrf_exempt
from utils.utils import get_sk, get_user
from django.contrib import messages
# from privat24.integration import Privat24Integration
from django.template.context_processors import csrf
from payment.models import Payment
from .liqpay import LiqPay
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from utils.utils import send_order_mail



CURRENT_DOMEN = settings.CURRENT_DOMEN

def pay(request):
  print(CURRENT_DOMEN)
  liqpay = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
  sk = get_sk(request)
  order = Order.objects.filter(sk=sk)
  if not order.exists():
    return redirect('/')
  else:
    order = order.reverse()[0]
  print(order.comments)
  params = {
      'action': 'pay',
      'amount': float(order.total_price),
      'currency': 'UAH',
      'description': str(order.comments),
      'order_id': str(order.id),
      'version': '3',
      'sandbox': 0, # sandbox mode, set to 1 to enable it
      'server_url': f'{CURRENT_DOMEN}pay_callback/', # url to callback view
  }
  signature = liqpay.cnb_signature(params)
  data = liqpay.cnb_data(params)
  return render(request, 'payment.html', {'signature': signature, 'data': data})


@csrf_exempt
def pay_callback(request):
    request.session.cycle_key()
    liqpay    = LiqPay(settings.LIQPAY_PUBLIC_KEY, settings.LIQPAY_PRIVATE_KEY)
    data      = request.POST.get('data')
    signature = request.POST.get('signature')
    sign      = liqpay.str_to_sign(settings.LIQPAY_PRIVATE_KEY + data + settings.LIQPAY_PRIVATE_KEY)
    response  = liqpay.decode_data_from_str(data)
    if sign == signature: print('callback is valid')
    print(response)
    action              = response.get('action', '')
    payment_id          = response.get('payment_id', '')
    status              = response.get('status', '')
    version             = response.get('version', '')
    type                = response.get('type', '')
    paytype             = response.get('paytype', '')
    public_key          = response.get('public_key', '')
    acq_id              = response.get('acq_id', '')
    order_id            = response.get('order_id', '')
    liqpay_order_id     = response.get('liqpay_order_id', '')
    description         = response.get('description', '')
    sender_phone        = response.get('sender_phone', '')
    sender_first_name   = response.get('sender_first_name', '')
    sender_last_name    = response.get('sender_last_name', '')
    sender_card_mask2   = response.get('sender_card_mask2', '')
    sender_card_bank    = response.get('sender_card_bank', '')
    sender_card_type    = response.get('sender_card_type', '')
    sender_card_country = response.get('sender_card_country', '')
    ip                  = response.get('ip', '')
    amount              = response.get('amount', '')
    currency            = response.get('currency', '')
    sender_commission   = response.get('sender_commission', '')
    receiver_commission = response.get('receiver_commission', '')
    agent_commission    = response.get('agent_commission', '')
    amount_debit        = response.get('amount_debit', '')
    amount_credit       = response.get('amount_credit', '')
    commission_debit    = response.get('commission_debit', '')
    commission_credit   = response.get('commission_credit', '')
    currency_debit      = response.get('currency_debit', '')
    currency_credit     = response.get('currency_credit', '')
    sender_bonus        = response.get('sender_bonus', '')
    amount_bonus        = response.get('amount_bonus', '')
    mpi_eci             = response.get('mpi_eci', '')
    is_3ds              = response.get('is_3ds', '')
    language            = response.get('language', '')
    create_date         = response.get('create_date', '')
    end_date            = response.get('end_date', '')
    transaction_id      = response.get('transaction_id', '')

    order = Order.objects.get(id=order_id)
    payment = Payment()
    payment.status   = status
    payment.status   = status
    payment.ip       = ip
    payment.amount   = amount
    payment.currency = currency
    payment.order    = Order.objects.get(pk=order_id)
    payment.sender_phone        = sender_phone
    payment.sender_first_name   = sender_first_name
    payment.sender_last_name    = sender_last_name
    payment.sender_card_mask2   = sender_card_mask2
    payment.sender_card_bank    = sender_card_bank
    payment.sender_card_type    = sender_card_type
    payment.sender_card_country = sender_card_country
    payment.save()
    order.save()
    send_order_mail()
    return redirect('thank_you')
    return HttpResponse()



from django import forms
# from privat24.forms import Privat24FrontForm


# Переопределение формы для запроса данных
# class CustomPrivat24Form(Privat24FrontForm):
#     amt = forms.CharField(widget=forms.HiddenInput())
#     ccy = forms.CharField(widget=forms.HiddenInput())



def payment(request):
  try:
    order = Order.objects.get(
      sk=get_sk(request), 
      ordered=False,
    )
  except Order.DoesNotExist:
    messages.warning(request, 'You do not have an active order')
    return redirect('/')
  ## if order.address:
  # p24 = Privat24Integration({
  #   # "amt": order.sum,
  #   "amt":order.total_price,
  #   "ccy": "UAH",
  #   "order": order.pk,
  #   "details": order.items.all(),
  #   'form_class': CustomPrivat24Form,
  # })
  form = p24.generate_form()
  context = {'integration':p24, 
              'form':form,
              'order':order
  }
  context.update(csrf(request))
  return render(request, 'payment.html', context)




@csrf_exempt
def thank_you(request):
  print(request.method)
  print('request.POST:', request.POST)
  if request.method == 'GET':
    # print('THANK_YOU********GET')
    # Это - то, что приходит через POST-ответ от приватбанка. 
    # С этим можно что-то сделать.
    # Hапример - привязать к заказу.
    privat = {
      'payment': ['amt=12&ccy=UAH&details=&ext_details=&pay_way=privat24&order=29&merchant=146917&state=test&date=230719172542&ref=test payment&payCountry=UA'], 
      'signature': ['ab962148ca66a9115c67729a47d5ca90185948f7'],
    }
    payment    = privat['payment'][0]
    signature  = privat['signature'][0]
    amt        = payment.split('&')[0].split('=')[1]
    ccy        = payment.split('&')[1].split('=')[1]
    details    = payment.split('&')[2].split('=')[1]
    ext_detail = payment.split('&')[3].split('=')[1]
    pay_way    = payment.split('&')[4].split('=')[1]
    order      = payment.split('&')[5].split('=')[1]
    merchant   = payment.split('&')[6].split('=')[1]
    state      = payment.split('&')[7].split('=')[1]
    date       = payment.split('&')[8].split('=')[1]
    ref        = payment.split('&')[9].split('=')[1]
    payCountry = payment.split('&')[10].split('=')[1]
    # print(signature); print(payment); print(amt); print(ccy); print(details); print(ext_detail); print(pay_way); print(order); print(merchant); print(state); print(date); print(ref); print(payCountry)
    # try:
    #   order = Order.objects.get(
    #     user=get_user(request),
    #     sk=get_sk(request),
    #     ordered=False
    #   )
    # except Order.DoesNotExist:
    #   messages.warning(request, 'You do not have and active order')
    #   return redirect('index')
    # order.ordered = True
    # order.payment = False
    # order.save()
    return render(request, 'thank_you.html', locals())
  if request.method == 'POST':
    # <QueryDict: {
    #   'payment': ['
    #     amt=12&
    #     ccy=UAH&
    #     details=&
    #     ext_details=&
    #     pay_way=privat24&
    #     order=29&
    #     merchant=146917&
    #     state=test&
    #     date=230719172542&
    #     ref=testpayment&
    #     payCountry=UA
    #   '], 
    #   'signature': ['ab962148ca66a9115c67729a47d5ca90185948f7']
    # }>
    payment = request.POST.get('payment')
    signature = request.POST.get('signature')
    amt = payment.split('&')[0].split('=')[1]
    ccy = payment.split('&')[1].split('=')[1]
    details = payment.split('&')[2].split('=')[1]
    ext_detail = payment.split('&')[3].split('=')[1]
    pay_way = payment.split('&')[4].split('=')[1]
    order = payment.split('&')[5].split('=')[1]
    merchant = payment.split('&')[6].split('=')[1]
    state = payment.split('&')[7].split('=')[1]
    date = payment.split('&')[8].split('=')[1]
    ref = payment.split('&')[9].split('=')[1]
    paycountry = payment.split('&')[10].split('=')[1]
    order = Order.objects.get(
      pk=order
    )
    print('order:', order)
    payment = Payment()
    payment.amt = amt
    payment.ccy = ccy
    payment.order = order
    payment.paycountry = paycountry
    payment.save()
    print('payment:', payment)
    order.payment = payment
    # order.payment = True
    order.ordered = True
    order.save()
    return render(request, 'thank_you.html', {})
