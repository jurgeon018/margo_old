from django import forms

PAYMENT_CHOICES = (
    ('manager', 'Предзаказ у менеджера'),
    ('privat', 'Предоплата на картку Приват24'),
)
CITY_CHOICES = (
  ('Львів', 'Львів'),
  ("Винники", "Винники"),
  ("Рудно", "Рудно"),
  ("Зимна Вода", "Зимна Вода"),
  ("Лапаївка", "Лапаївка"),
  ("Холодновідка", "Холодновідка"),
  ("Старе", "Старе Рудно"),
  ("Скнилів", "Скнилів"),
  ("Солонка", "Солонка"),
  ("Сокільники", "Сокільники"),
  ("Брюховичі", "Брюховичі"),
)
SHIPPING_CHOICES = (
  ('Адресна доставка','Адресна доставка'),
  ('Перевізник','Перевізник'),
  ('По Львову','По Львову'),
  ('Поштомат','Поштомат'),
)

class CheckoutForm(forms.Form):
  name =  forms.CharField(label="П.І.Б", max_length=100, required=False, widget=forms.TextInput(attrs={  'style':'color:black;'}))
  email =  forms.CharField(label="Email-адреса", max_length=100, required=False, widget=forms.TextInput(attrs={  'style':'color:black;'}))
  address =  forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs={  'style':'color:black;'}))
  phone           = forms.CharField(
    label="Номер телефону", 
    widget=forms.TextInput(
      attrs={
        'style':'color:black;',
        # 'placeholder':"Телефон",
      }
    ),
  )
  city            = forms.ChoiceField(choices=CITY_CHOICES, label="Місто", widget=forms.TextInput(attrs={  'style':'color:black;'}))
  street          = forms.CharField(label="Адреса доставки", widget=forms.TextInput(attrs={  'style':'color:black;'}))
  # shipping        = forms.ChoiceField(choices=SHIPPING_CHOICES, label="Спосіб доставки", widget=forms.Select(attrs={  'style':'color:black;'}))
  shipping        = forms.ChoiceField(choices=SHIPPING_CHOICES, label="Спосіб доставки",widget=forms.Select(attrs={  'style':'color:black;'}))
  payment_option  = forms.ChoiceField(choices=PAYMENT_CHOICES, widget=forms.Select(attrs={  'style':'color:black;'}), label="Спосіб оплати")
  comment         = forms.CharField(label="Ваше повідомлення(коментар)", widget=forms.Textarea(attrs={'rows': 5, 'cols': 40,'style':'color:black;'}))