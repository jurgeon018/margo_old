from django.urls import path
from django.shortcuts import (render, get_object_or_404, reverse, redirect)
from order.forms import (CheckoutForm)
from django.contrib import messages
from order.models import Order, OrderItem
from cart.models import CartItem
from django.views.generic import View
from utils.utils import get_sk, get_user
from django.http import HttpResponse, JsonResponse
from utils.utils import send_order_mail
from constructor.models import Napkin


def order_items(request):
  flag = 'items'
  if request.method == 'GET':
    cart_items = CartItem.objects.filter(sk=get_sk(request), ordered=False).exists()
    if not cart_items:
      referer = request.META.get('HTTP_REFERER', '/')
      return redirect(referer)
    return render(request, 'checkout.html', locals())
  sk            = get_sk(request)
  name          = request.POST.get('name', '')
  phone         = request.POST.get('phone', '')
  email         = request.POST.get('email', '')
  comments      = request.POST.get('comment', '')
  address       = request.POST.get('address', '')
  payment       = request.POST.get('payment', '')
  delivery      = request.POST.get('delivery', '')
  order         = Order()
  order.name    = name
  order.email   = email
  order.address = address
  order.phone   = phone
  order.comments= comments 
  order.payment_way = payment
  order.delivery= delivery
  order.sk      = sk
  order.save()

  create_order_items(request, order)
  order.save()
  if payment == 'privat':
    return redirect("pay")
  elif payment == 'manager':
    send_order_mail()
    return redirect('thank_you')



def order_napkin(request):
  flag = 'napkin'
  if request.method == 'GET':
    napkins = Napkin.objects.filter(sk=get_sk(request), ordered=False).exists()
    if not napkins:
      referer = request.META.get('HTTP_REFERER', '/')
      return redirect(referer)
    return render(request, 'checkout.html', locals())
  sk            = get_sk(request)
  name          = request.POST.get('name', '')
  phone         = request.POST.get('phone', '')
  email         = request.POST.get('email', '')
  comments      = request.POST.get('comment', '')
  address       = request.POST.get('address', '')
  payment       = request.POST.get('payment', '')
  delivery      = request.POST.get('delivery', '')
  napkin         = Napkin.objects.get(sk=sk, ordered=False)
  napkin.name    = name
  napkin.email   = email
  napkin.address = address
  napkin.phone   = phone
  napkin.comments= comments 
  napkin.sk      = sk
  napkin.ordered=True
  # 111111111111@gmail.com
  napkin.save()
  send_order_mail()
  # send_napkin_mail()
  return redirect('thank_you')


def create_order(request):
  return order, payment


def create_order_items(request, order):
  sk = get_sk(request)
  total_price = 0
  for cart_item in CartItem.objects.filter(ordered=False, sk=sk):
    total_price += cart_item.total_price
  order.total_price = total_price

  cart_items = CartItem.objects.filter(sk=sk)
  if request.user.is_authenticated:
    order.user = request.user
    cart_items.update(user=request.user)

  order.save()
  cart_items.update(order=order)

  for cart_item in CartItem.objects.filter(sk=sk, ordered=False):
    order_item = OrderItem()
    order_item.item = cart_item.item 
    order_item.user = cart_item.user 
    order_item.sk = cart_item.sk 
    order_item.order = cart_item.order 
    # order_item.size = cart_item.size 
    order_item.quantity = cart_item.quantity 
    order_item.price_per_item = cart_item.price_per_item 
    order_item.total_price = cart_item.total_price 
    order_item.save()
  cart_items.update(ordered=True)
  # cart_items.delete()
