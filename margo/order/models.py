from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django_countries.fields import CountryField
from django_countries.widgets import CountrySelectWidget
from django.utils.translation import gettext as _
from cart.models import CartItem
User = get_user_model()


ORDER_STATUS_CHOICES = (
	(_('Принят в обработку'), _('Принят в обработку')),
	(_('Выполняется'), _('Выполняется')),
	(_('Выполнен'), _('Выполнен')),
	# (_('Оплачен'), _('Оплачен')),
)


class Status(models.Model):
  name = models.CharField(max_length=24, blank=True, null=True, default=None)
  is_active = models.BooleanField(default=True)
  created = models.DateTimeField(auto_now_add=True, auto_now=False)
  updated = models.DateTimeField(auto_now_add=False, auto_now=True)
  def __str__(self):
    return "Статус %s" % self.name
  class Meta: verbose_name = 'Статус'; verbose_name_plural = 'Статусы'; 


class OrderItemManager(models.Manager):
  def all(self):
    return super(OrderItemManager, self).get_queryset().filter(ordered=False)


class OrderItem(models.Model):
  user           = models.ForeignKey(User, verbose_name='Пользователь',on_delete=models.CASCADE, blank=True, null=True)
  sk             = models.CharField(max_length=120, verbose_name='Ключ сессии',blank=True, null=True)
  item           = models.ForeignKey('item.Item', verbose_name='Товар',on_delete=models.CASCADE, blank=True, null=True)
  order          = models.ForeignKey('order.Order', related_name='items', verbose_name='Заказ',on_delete=models.CASCADE, blank=True, null=True)
  size           = models.CharField(verbose_name='Размер',max_length=20, blank=True, null=True)
  quantity       = models.IntegerField(verbose_name='Количество',default=1)
  price_per_item = models.DecimalField(verbose_name='Цена за шт.',max_digits=10, decimal_places=2, blank=True, null=True)
  total_price    = models.DecimalField(verbose_name='Суммарная стоимость',max_digits=10, decimal_places=2, blank=True, null=True)
  ordered        = models.BooleanField(verbose_name='Заказан ли?',default=False, blank=True, null=True)
  created        = models.DateTimeField(verbose_name='Дата создания',auto_now_add=True, auto_now=False, blank=True, null=True)
  updated        = models.DateTimeField(verbose_name='Дата обновления',auto_now_add=False, auto_now=True, blank=True, null=True)
  # objects        = OrderItemManager()
  def __str__(self):
    return f'{self.item.name}, {self.quantity}штука, {self.total_price}'
  def save(self, *args, **kwargs):
    price_per_item = self.item.price
    self.price_per_item = price_per_item
    self.total_price = self.quantity * price_per_item
    super().save(*args, **kwargs)
  class Meta: verbose_name = 'Товар в заказе'; verbose_name_plural = 'Товары в заказе'; 


class Order(models.Model):
  user        = models.ForeignKey(User, verbose_name='Пользователь',on_delete=models.CASCADE, blank=True, null=True)
  sk          = models.CharField(verbose_name='Ключ сессии',max_length=120)
  total_price = models.DecimalField(verbose_name='Стоимость',max_digits=10, decimal_places=2, default=0)
  name        = models.CharField(verbose_name='Имя',max_length=120, blank=True, null=True)
  email       = models.CharField(max_length=120, blank=True, null=True)
  phone       = models.CharField(verbose_name='Номер телефона',max_length=120, blank=True, null=True)
  address     = models.CharField(verbose_name='Адрес',max_length=120, blank=True, null=True)
  comments    = models.TextField(verbose_name='Коментарии',blank=True, null=True, default=None)
  delivery    = models.CharField(max_length=120, verbose_name="Способ доставки", blank=True, null=True, ) 
  payment_way = models.CharField(max_length=120, verbose_name="Оплата", blank=True, null=True, ) 
  status      = models.CharField(max_length=120, choices=ORDER_STATUS_CHOICES, blank=True, null=True) 
  created     = models.DateTimeField(verbose_name='Дата создания',auto_now_add=True, auto_now=False, blank=True, null=True)
  updated     = models.DateTimeField(verbose_name='Дата обновления',auto_now_add=False, auto_now=True, blank=True, null=True)
  class Meta: verbose_name = 'Заказ'; verbose_name_plural = 'Заказы'; 
  def __str__(self):
    return f'{self.phone}|{self.name}|{self.email}|{self.address}' 