from django.contrib import admin
from order.models import Order, Status, OrderItem
from payment.models import Payment
from django.forms import widgets
from django.db import models
from constructor.models import Napkin
# from rollyourown.seo.admin import register_seo_admin
# from order.seo import MyMetadata


# def make_refund_accepted(modeladmin, request, queryset):
#   ''' Меняет на всех выделенных товарах поле '''
#   queryset.update(refund_requested=False, refund_granted=True)
# make_refund_accepted.short_description = 'Update orders to refund granted'

# register_seo_admin(admin.site, MyMetadata)


class OrderItemAdmin (admin.ModelAdmin):
    list_display = [field.name for field in OrderItem._meta.fields]
    exclude = ['sk', 'user']
    class Meta:
        model = OrderItem
admin.site.register(OrderItem, OrderItemAdmin)


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    extra = 0
    exclude = ['sk','user', 'ordered']
    readonly_fields = ['size','item', 'quantity','price_per_item','total_price']
    can_delete = False


class PaymentAdmin(admin.ModelAdmin):
    # list_display = [field.name for field in OrderItem._meta.fields]
    list_display = ['user', 'amt']
    list_display = [
    'status',
    'amount',
    'status',
    'ip',
    'order',
    'sender_phone',
    'sender_first_name',
    'sender_last_name',
    'sender_card_mask2',
    'sender_card_bank',
    'sender_card_type',
    'sender_card_country',
    ]
    # exclude = ['sk', 'user']
    class Meta:
        model = Payment
admin.site.register(Payment, PaymentAdmin)



class PaymentInOrderInline(admin.TabularInline):
    model = Payment
    extra = 0
    exclude = ['sk','user', 'ordered']
    # readonly_fields = ['size','item', 'quantity','price_per_item','total_price']
    can_delete = False

class NapkinInline(admin.TabularInline):
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
    model = Napkin
    extra = 0
    fields = ['logo', 'final','sk', ]
    # can_delete = False


class OrderAdmin(admin.ModelAdmin):
    class Meta:
        model = Order
    inlines = [
        OrderItemInline,
        PaymentInOrderInline,
        # NapkinInline
    ]
#   list_display = ['user','session_key', 'ordered', 'being_delivered', 'received','refund_requested', 'refund_granted']
    list_display = [field.name for field in Order._meta.fields]
    # list_display += ['OrderItem_set',]
    exclude=['sk', 'user','ordered_date']
    search_fields = ['user__username','ref_code']
#   list_filter = ['user','session_key', 'ordered', 'being_delivered', 'received','refund_requested', 'refund_granted']
    # actions = [make_refund_accepted]  
    formfield_overrides = {
        models.ManyToManyField: {'widget': widgets.CheckboxSelectMultiple},
        # models.DateTimeField: {'widget': widgets.TextInput}
    }
    readonly_fields = ['name','email','phone','address','comments','total_price']


    # fieldsets = [
    #     (None,                {'fields': ['title', 'slug']}),
    #     ('Other information', {'fields': ['content', 'profile', 'thumbnail', 'tags', 'categories', 'featured', 'previous_post', 'next_post'], 
    #                            'classes':['collapse']}),
    # ]
    # list_display = ['id','title', 'slug', 'profile', 'timestamp', 'featured']
    # list_display_links = ['id']#, 'title', 'profile', 'timestamp']
    # list_editable = ['title', 'slug','profile','featured']
    # raw_id_fields = ['previous_post', 'next_post']
    # # save_as = True # сохранить как новый обьект
    # save_as = False # сохранить и добавить другой обьект


admin.site.register(Order, OrderAdmin)

admin.site.register(Status)