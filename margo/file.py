# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AccountEmailaddress(models.Model):
    verified = models.BooleanField()
    primary = models.BooleanField()
    user = models.ForeignKey('UserUser', models.DO_NOTHING)
    email = models.CharField(unique=True, max_length=254)

    class Meta:
        managed = False
        db_table = 'account_emailaddress'


class AccountEmailconfirmation(models.Model):
    created = models.DateTimeField()
    sent = models.DateTimeField(blank=True, null=True)
    key = models.CharField(unique=True, max_length=64)
    email_address = models.ForeignKey(AccountEmailaddress, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'account_emailconfirmation'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)
    name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class CartCartitem(models.Model):
    sk = models.CharField(max_length=120, blank=True, null=True)
    quantity = models.IntegerField()
    price_per_item = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    total_price = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    ordered = models.BooleanField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    item = models.ForeignKey('ItemItem', models.DO_NOTHING, blank=True, null=True)
    order = models.ForeignKey('OrderOrder', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('UserUser', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cart_cartitem'


class CartFavouritem(models.Model):
    sk = models.CharField(max_length=120, blank=True, null=True)
    item = models.ForeignKey('ItemItem', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('UserUser', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cart_favouritem'


class ConstructorColour(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)
    code = models.CharField(max_length=20, blank=True, null=True)
    rgb = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'constructor_colour'


class ConstructorEmbossing(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=20, blank=True, null=True)
    image = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'constructor_embossing'


class ConstructorFolding(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=20, blank=True, null=True)
    image = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'constructor_folding'


class ConstructorFormfactor(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'constructor_formfactor'


class ConstructorNapkin(models.Model):
    sk = models.CharField(max_length=120, blank=True, null=True)
    final = models.CharField(max_length=100, blank=True, null=True)
    logo = models.CharField(max_length=100, blank=True, null=True)
    ordered = models.BooleanField()
    template = models.ForeignKey('ConstructorNapkintemplate', models.DO_NOTHING, blank=True, null=True)
    address = models.CharField(max_length=120, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    email = models.CharField(max_length=120, blank=True, null=True)
    name = models.CharField(max_length=120, blank=True, null=True)
    phone = models.CharField(max_length=120, blank=True, null=True)
    status = models.CharField(max_length=120, blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'constructor_napkin'


class ConstructorNapkintemplate(models.Model):
    image = models.CharField(max_length=100, blank=True, null=True)
    default = models.BooleanField()
    colour = models.ForeignKey(ConstructorColour, models.DO_NOTHING, blank=True, null=True)
    embossing = models.ForeignKey(ConstructorEmbossing, models.DO_NOTHING, blank=True, null=True)
    folding = models.ForeignKey(ConstructorFolding, models.DO_NOTHING, blank=True, null=True)
    form_factor = models.ForeignKey(ConstructorFormfactor, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'constructor_napkintemplate'


class CorePage(models.Model):
    name = models.CharField(max_length=120, blank=True, null=True)
    title = models.CharField(max_length=120, blank=True, null=True)
    desc = models.CharField(max_length=120, blank=True, null=True)
    url = models.CharField(max_length=120, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_page'


class CoreUsercontact(models.Model):
    request_type = models.CharField(max_length=120)
    email = models.CharField(max_length=254)
    phone = models.CharField(max_length=20)
    text = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'core_usercontact'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('UserUser', models.DO_NOTHING)
    action_flag = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoFlatpage(models.Model):
    url = models.CharField(max_length=100)
    title = models.CharField(max_length=200)
    content = models.TextField()
    enable_comments = models.BooleanField()
    template_name = models.CharField(max_length=70)
    registration_required = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'django_flatpage'


class DjangoFlatpageSites(models.Model):
    flatpage = models.ForeignKey(DjangoFlatpage, models.DO_NOTHING)
    site = models.ForeignKey('DjangoSite', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_flatpage_sites'
        unique_together = (('flatpage', 'site'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoRedirect(models.Model):
    site = models.ForeignKey('DjangoSite', models.DO_NOTHING)
    old_path = models.CharField(max_length=200)
    new_path = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'django_redirect'
        unique_together = (('site', 'old_path'),)


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    name = models.CharField(max_length=50)
    domain = models.CharField(unique=True, max_length=100)

    class Meta:
        managed = False
        db_table = 'django_site'


class ItemCategory(models.Model):
    title = models.CharField(max_length=120, blank=True, null=True)
    slug = models.CharField(max_length=50, blank=True, null=True)
    is_active = models.BooleanField()
    image = models.CharField(max_length=100, blank=True, null=True)
    title_en = models.CharField(max_length=120, blank=True, null=True)
    title_ru = models.CharField(max_length=120, blank=True, null=True)
    title_uk = models.CharField(max_length=120, blank=True, null=True)
    code = models.CharField(unique=True, max_length=20, blank=True, null=True)
    subcat = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'item_category'


class ItemItem(models.Model):
    name = models.CharField(max_length=120, blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    discount_price = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    articul = models.CharField(max_length=20, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    slug = models.CharField(unique=True, max_length=50, blank=True, null=True)
    available = models.BooleanField()
    is_active = models.BooleanField()
    created = models.DateTimeField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    layers = models.CharField(max_length=10, blank=True, null=True)
    description_en = models.TextField(blank=True, null=True)
    description_ru = models.TextField(blank=True, null=True)
    description_uk = models.TextField(blank=True, null=True)
    name_en = models.CharField(max_length=120, blank=True, null=True)
    name_ru = models.CharField(max_length=120, blank=True, null=True)
    name_uk = models.CharField(max_length=120, blank=True, null=True)
    size = models.ForeignKey('ItemSize', models.DO_NOTHING, blank=True, null=True)
    amount = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'item_item'


class ItemItemCategory(models.Model):
    item = models.ForeignKey(ItemItem, models.DO_NOTHING)
    category = models.ForeignKey(ItemCategory, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'item_item_category'
        unique_together = (('item', 'category'),)


class ItemItemimage(models.Model):
    image = models.CharField(max_length=100, blank=True, null=True)
    alt = models.CharField(max_length=120, blank=True, null=True)
    is_main = models.BooleanField()
    is_active = models.BooleanField()
    created = models.DateTimeField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    item = models.ForeignKey(ItemItem, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'item_itemimage'


class ItemSize(models.Model):
    name = models.CharField(max_length=120, blank=True, null=True)
    name_en = models.CharField(max_length=120, blank=True, null=True)
    name_ru = models.CharField(max_length=120, blank=True, null=True)
    name_uk = models.CharField(max_length=120, blank=True, null=True)
    code = models.CharField(unique=True, max_length=120, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'item_size'


class OrderOrder(models.Model):
    sk = models.CharField(max_length=120)
    total_price = models.DecimalField(max_digits=10, decimal_places=5)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    created = models.DateTimeField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=120, blank=True, null=True)
    email = models.CharField(max_length=120, blank=True, null=True)
    phone = models.CharField(max_length=120, blank=True, null=True)
    address = models.CharField(max_length=120, blank=True, null=True)
    comments = models.TextField(blank=True, null=True)
    user = models.ForeignKey('UserUser', models.DO_NOTHING, blank=True, null=True)
    status = models.CharField(max_length=120, blank=True, null=True)
    delivery = models.CharField(max_length=120, blank=True, null=True)
    payment_way = models.CharField(max_length=120, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'order_order'


class OrderOrderitem(models.Model):
    sk = models.CharField(max_length=120, blank=True, null=True)
    size = models.CharField(max_length=20, blank=True, null=True)
    quantity = models.IntegerField()
    price_per_item = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    total_price = models.DecimalField(max_digits=10, decimal_places=5, blank=True, null=True)  # max_digits and decimal_places have been guessed, as this database handles decimal fields as float
    ordered = models.BooleanField(blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    updated = models.DateTimeField(blank=True, null=True)
    item = models.ForeignKey(ItemItem, models.DO_NOTHING, blank=True, null=True)
    order = models.ForeignKey(OrderOrder, models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('UserUser', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'order_orderitem'


class OrderStatus(models.Model):
    name = models.CharField(max_length=24, blank=True, null=True)
    is_active = models.BooleanField()
    created = models.DateTimeField()
    updated = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'order_status'


class PaymentPayment(models.Model):
    status = models.CharField(max_length=120, blank=True, null=True)
    ip = models.CharField(max_length=120, blank=True, null=True)
    amount = models.CharField(max_length=120, blank=True, null=True)
    currency = models.CharField(max_length=120, blank=True, null=True)
    sender_phone = models.CharField(max_length=120, blank=True, null=True)
    sender_first_name = models.CharField(max_length=120, blank=True, null=True)
    sender_last_name = models.CharField(max_length=120, blank=True, null=True)
    sender_card_mask2 = models.CharField(max_length=120, blank=True, null=True)
    sender_card_bank = models.CharField(max_length=120, blank=True, null=True)
    sender_card_type = models.CharField(max_length=120, blank=True, null=True)
    sender_card_country = models.CharField(max_length=120, blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    order = models.ForeignKey(OrderOrder, models.DO_NOTHING, unique=True, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'payment_payment'


class SocialaccountSocialaccount(models.Model):
    provider = models.CharField(max_length=30)
    uid = models.CharField(max_length=191)
    last_login = models.DateTimeField()
    date_joined = models.DateTimeField()
    user = models.ForeignKey('UserUser', models.DO_NOTHING)
    extra_data = models.TextField()

    class Meta:
        managed = False
        db_table = 'socialaccount_socialaccount'
        unique_together = (('provider', 'uid'),)


class SocialaccountSocialapp(models.Model):
    provider = models.CharField(max_length=30)
    name = models.CharField(max_length=40)
    client_id = models.CharField(max_length=191)
    key = models.CharField(max_length=191)
    secret = models.CharField(max_length=191)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialapp'


class SocialaccountSocialappSites(models.Model):
    socialapp = models.ForeignKey(SocialaccountSocialapp, models.DO_NOTHING)
    site = models.ForeignKey(DjangoSite, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialapp_sites'
        unique_together = (('socialapp', 'site'),)


class SocialaccountSocialtoken(models.Model):
    token = models.TextField()
    token_secret = models.TextField()
    expires_at = models.DateTimeField(blank=True, null=True)
    account = models.ForeignKey(SocialaccountSocialaccount, models.DO_NOTHING)
    app = models.ForeignKey(SocialaccountSocialapp, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'socialaccount_socialtoken'
        unique_together = (('app', 'account'),)


class UserUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    phone_number = models.CharField(max_length=120, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_user'


class UserUserGroups(models.Model):
    user = models.ForeignKey(UserUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_user_groups'
        unique_together = (('user', 'group'),)


class UserUserUserPermissions(models.Model):
    user = models.ForeignKey(UserUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_user_user_permissions'
        unique_together = (('user', 'permission'),)
