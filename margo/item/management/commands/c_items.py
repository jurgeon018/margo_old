from django.core.management.base import BaseCommand
from item.models import Item, ItemImage, Category, Size
import random
import datetime 
import json 
import csv


class Command(BaseCommand):
  def add_arguments(self, parser):
    parser.add_argument(
      'file_name', 
      type=str, 
      help='File, that contains the main item\'s information'
    )
  def handle(self, *args, **kwargs):
    file = open(f"{kwargs['file_name']}")
    products = csv.reader(file)
    for product in list(products)[1:]:
      name     = product[0]
      name_uk  = product[1]
      name_ru  = product[2]
      name_en  = product[3]
      category = product[4]
      size     = product[5]
      amount   = product[6]
      layers   = product[7]
      url      = product[8]
      price    = 1
      cat, _   = Category.objects.get_or_create(code=category)
      size, _  = Size.objects.get_or_create(code=size)
      item, _  = Item.objects.get_or_create(
        name=name,
        name_ru=name_ru,
        name_en=name_en,
        name_uk=name_uk,
      )
      image, _ = ItemImage.objects.get_or_create(image=url, item=item)
      item.size     = size
      try:
        item.category = cat
      except:
        item.category.add(cat)
      item.articul  = str(item.pk).zfill((3))
      item.layers   = layers
      item.amount   = amount
      item.price    = price
      item.discount_price = price
      item.save()
      print(item, item.pk)
    self.stdout.write(self.style.SUCCESS('Data imported successfully'))
