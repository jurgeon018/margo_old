from modeltranslation.translator import translator, TranslationOptions, register
from .models import *

@register(Item)
class ItemTranslationOptions(TranslationOptions):
    fields = (
        'name',
        'description',
    )

@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = (
        'title',
        # 'slug'
    )

@register(Size)
class SizeTranslationOptions(TranslationOptions):
    fields = (
        'name',
    )
