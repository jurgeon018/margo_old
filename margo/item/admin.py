from django.contrib import admin
from item.models import *
from import_export.admin import ImportExportModelAdmin
# from mptt.admin import MPTTModelAdmin
from modeltranslation.admin import TranslationAdmin

class ProductImageInline(admin.TabularInline):
    model = ItemImage
    extra = 0


# class ItemImageAdmin (admin.ModelAdmin):
class ItemImageAdmin (ImportExportModelAdmin):
    list_display = [field.name for field in Item._meta.fields]
    # exclude = ['sk', 'user']
    class Meta:
        model = ItemImage


# class ItemAdmin (admin.ModelAdmin):
class ItemAdmin(ImportExportModelAdmin, TranslationAdmin):
    list_display = [field.name for field in Item._meta.fields]
    exclude = ['sk', 'user']
    inlines = [ProductImageInline]
    class Meta:
        model = Item
    class Media:
        js = (
    #         '/static/modeltranslation/js/force_jquery.js',
    #         'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
    #         '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }


admin.site.register(Item, ItemAdmin)
admin.site.register(Category)
admin.site.register(ItemImage)
admin.site.register(Size)