from django.db import models
from django.conf import settings 
from django.db.models.signals import post_save, pre_save
from django.utils.text import slugify
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from PIL import Image
from transliterate import translit


class ItemManager(models.Manager):
  def all(self):
    return super(ItemManager, self).get_queryset().filter(is_active=True)


class Category(models.Model):
  subcat    = models.ForeignKey('self',blank=True, null=True, on_delete=models.CASCADE)
  code      = models.CharField(max_length=20, blank=True, null=True, unique=True)
  title     = models.CharField(max_length=120, blank=True, null=True)
  slug      = models.SlugField(blank=True, null=True)
  is_active = models.BooleanField(default=True)
  image     = models.ImageField(upload_to='cat_pics', blank=True, null=True)
  def __str__(self): return self.code
  class Meta: verbose_name = 'Категорія'; verbose_name_plural = 'Категорії'; 


class Size(models.Model):
  code = models.CharField(max_length=120, blank=True, null=True, unique=True)
  name = models.CharField(max_length=120, blank=True, null=True)
  class Meta: verbose_name=_("Розмір"); verbose_name_plural=_("Розміри"); 
  def __str__(self): return self.code


class ItemImage(models.Model):
  item      = models.ForeignKey('item.Item', verbose_name='Товар',related_name='image',blank=True, null=True, default=None, on_delete=models.CASCADE)
  image     = models.ImageField(verbose_name=_('Ссылка изображения'), upload_to='item_pics', blank=True, null=True)
  alt       = models.CharField(max_length=120, blank=True, null=True)
  is_main   = models.BooleanField(verbose_name='Главное изображение',default=False)
  is_active = models.BooleanField(verbose_name='Активное изображение',default=True)
  created   = models.DateTimeField(verbose_name='Дата создания',auto_now_add=True, auto_now=False, blank=True, null=True)
  updated   = models.DateTimeField(verbose_name='Дата обновления',auto_now_add=False, auto_now=True, blank=True, null=True)
  def __str__(self):
      return "%s" % self.image.url
  def save(self, *args, **kwargs):
    super().save(*args, **kwargs)
    img = Image.open(self.image.path)
    # img.thumbnail((1000, 100))
    img = img.resize((400, 400), Image.ANTIALIAS)
    # img.resize((400, 400), Image.ANTIALIAS) # doesnt work
    img.save(self.image.path)
  class Meta: verbose_name = _('Изображение товара'); verbose_name_plural = _('Изображения товара'); 


class Item(models.Model):
  name           = models.CharField(verbose_name=_("Назва"), max_length=120, blank=True, null=True)
  size           = models.ForeignKey(Size ,verbose_name=_("Размер"), on_delete=models.CASCADE, blank=True, null=True)
  articul        = models.CharField(verbose_name=_("Артикул"), max_length=20, blank=True, null=True)   
  layers         = models.CharField(max_length=10, blank=True, null=True)
  amount         = models.CharField(max_length=20, blank=True, null=True)
  # category       = models.ForeignKey(Category, verbose_name=_("Категорія"), blank=True, null=True, on_delete=models.CASCADE)    
  category       = models.ManyToManyField(Category, verbose_name=_("Категорія"), blank=True, null=True)    
  description    = models.TextField(verbose_name=_("Опис"), blank=True, null=True)
  slug           = models.SlugField(verbose_name=_("Slug"), blank=True, null=True, unique=True)
  price          = models.DecimalField(verbose_name=_("Ціна"), max_digits=10, decimal_places=2, default=0, blank=True, null=True)
  discount_price = models.DecimalField(verbose_name=_("Скидочна ціна"), max_digits=10, decimal_places=2, default=0, blank=True, null=True)
  available      = models.BooleanField(verbose_name=_("Наявність"), default=True)
  is_active      = models.BooleanField(verbose_name=_("Чи активний"), default=True)
  created        = models.DateTimeField(verbose_name=_("Створений"), auto_now_add=True,  auto_now=False, blank=True, null=True)
  updated        = models.DateTimeField(verbose_name=_("Оновлений"), auto_now_add=False, auto_now=True,  blank=True, null=True)
  objects        = ItemManager()
  class Meta: verbose_name = _('Товар'); verbose_name_plural = _('Товары'); 
  def __str__(self):
    return f"{self.name}"


def post_save_item_slug(sender, instance, *args, **kwargs):
  if not instance.slug:
    print('not')
    try:
      slug = slugify(translit(instance.name, reversed=True))
    except:
      slug = slugify(instance.name)
    instance.slug = slug + str(instance.id)
    instance.save()
post_save.connect(post_save_item_slug, sender=Item)