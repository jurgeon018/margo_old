from django.http import HttpResponse, JsonResponse
from rest_framework import serializers
from .models import Item, ItemImage, Category
from django.views.decorators.csrf import csrf_exempt
from utils.utils import get_sk
from cart.models import FavourItem
from django.core.paginator import Paginator
from django.shortcuts import render


class ItemImageSerializer(serializers.ModelSerializer):
  class Meta:
    model = ItemImage
    exclude = []



class ItemSerializer(serializers.ModelSerializer):
  # image = serializers.StringRelatedField(many=True, read_only=True)
  image = ItemImageSerializer(many=True, read_only=True)
  class Meta:
    model = Item
    exclude = []


@csrf_exempt
def get_items(request, slug=None):
  items = Item.objects.all()
  page_number = request.GET.get('page', 1)
  per_page    = request.GET.get('per_page', 8)
  sorting     = request.GET.get('sorting', '-discount_price')
  if sorting == 'undefined': 
    sorting = '-discount_price'
  try:
    raw = sorting.split('?')
    sorting= raw[0]
    page_number = int(raw[1].split('=')[-1])
  except Exception as e1:
    print('e1:', e1)
  try:
    page_number = request.GET['?page']
  except Exception as e2:
    print('e2:', e2)
  try:
    page_number = request.GET['page']
  except Exception as e3:
    print('e3:', e3)

  items = items.order_by(sorting)
  size        = request.GET.get('size', '')
  if size != 'all' and size!='' and size != None:
    items = items.filter(size__code=size)
  layers      = request.GET.get('layers', '')
  if layers != 'all' and layers!='' and layers != None:
    items = items.filter(layers=layers)
  print(size)
  print(layers)
  if slug: 
    cat   = Category.objects.get(slug=slug)
    items = items.filter(category=cat)


  items_in_favours = []
  for item in items:
    favour_item = FavourItem.objects.filter(
      sk=get_sk(request),
      item=Item.objects.get(id=item.id),
    )
    if favour_item.exists():
      items_in_favours.append(item.id)



  data           = ItemSerializer(items, many=True, read_only=True).data
  page           = Paginator(items, per_page=per_page).get_page(page_number)
  paginated_data = ItemSerializer(page, many=True, read_only=True).data
  is_paginated   = page.has_other_pages()
  current_page   = page.number
  last_page      = page.paginator.num_pages
  page_range     = page.paginator.page_range
  first_url      = f'?page=1'
  has_prev       = page.has_previous()
  has_next       = page.has_next()
  next_url       = f'?page={page.next_page_number()}' if has_next else ''
  prev_url       = f'?page={page.previous_page_number()}' if has_prev else ''
  last_url       = f'?page={last_page}'
  response       = {
    'items':           data,
    'items_in_favours':items_in_favours,
    'paginated_items': paginated_data,
    'is_paginated':    is_paginated,
    'current_page':    current_page,
    'page_range':      list(page_range),
    'last_page':       last_page,
    'first_url':       first_url,
    'next_url':        next_url,
    'prev_url':        prev_url,
    'last_url':        last_url,
    'has_prev':        has_prev,
    'has_next':        has_next,
  }
  # if request.is_ajax():
  return JsonResponse(response)
  return render(request, 'fetch_spinner_test.html', locals())
  return render(request, 'pagination_test.html', locals())

HTML_FOR_PAGINATION = ''' 
{% for item in page %}
    {{ item.name }}<br>
{% endfor %}
{% if has_prev %}
    <a href="{{first_url}}">first</a>
    <a href="{{prev_url}}">previous</a>
{% endif %}
Page {{ current_page }} of {{ last_page }}.
{% if has_next %}
    <a href="{{next_url}}">next</a>
    <a href="{{last_url}}">last</a>
{% endif %}
'''


@csrf_exempt
def is_favour(request):
  item_id = request.POST.get('item_id','')
  favour_item = FavourItem.objects.filter(
    sk=get_sk(request),
    item=Item.objects.get(id=item_id),
  )
  response = 'yes' if favour_item.exists() else 'no'
  return HttpResponse(response)