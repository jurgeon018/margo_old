from django import template
from order.models import Order, CartItem


register = template.Library()

@register.filter
def cart_item_count(request):
  if request.user.is_authenticated:
    qs = Order.objects.filter(user=request.user, ordered=False)
  elif request.user.is_anonymous:
    session_key = request.session.session_key
    if not session_key: request.session.cycle_key()
    qs = Order.objects.filter(session_key=session_key, ordered=False)
  return qs[0].items.count() if qs.exists() else 0


@register.filter
def cart_item_count(request):
  if request.user.is_authenticated:
    qs = CartItem.objects.filter(user=request.user, ordered=False)
  elif request.user.is_anonymous:
    session_key = request.session.session_key
    if not session_key: request.session.cycle_key()
    qs = CartItem.objects.filter(session_key=session_key, ordered=False)
  return qs.count() if qs.exists() else 0


