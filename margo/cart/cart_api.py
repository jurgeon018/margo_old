from django.template.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic import View
# from privat24.integration import Privat24Integration
from django.shortcuts import redirect, get_object_or_404
from django.contrib import admin
from django.contrib import messages
from django.utils import timezone
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from utils.utils import get_sk, get_user
from item.models import Item, ItemImage
from cart.models import CartItem, FavourItem
import random 
import string
from rest_framework import serializers
from django.views.decorators.csrf import csrf_exempt

# CART_ITEMS


class ItemImageSerializer(serializers.ModelSerializer):
  class Meta:
    model = ItemImage
    exclude = []


# https://www.django-rest-framework.org/api-guide/relations/#nested-relationships
class ItemSerializer(serializers.ModelSerializer):
  # image = serializers.StringRelatedField(many=True, read_only=True)
  image = ItemImageSerializer(many=True, read_only=True)
  class Meta:
    model = Item
    exclude = []


class CartItemSerializer(serializers.ModelSerializer):
  item = ItemSerializer()
  class Meta:
    model = CartItem
    exclude = []


@csrf_exempt
def add_cart_item(request):
  quantity = request.POST.get('quantity')
  # size     = request.POST.get('size','24x24')
  id       = request.POST.get('id')
  item     = get_object_or_404(Item, pk=int(id))
  try: quantity = int(quantity)
  except: quantity = 1
  cart_item, created = CartItem.objects.get_or_create(
    sk=get_sk(request),
    item=item,  # item__id=item.id,
    # size=size,
    ordered=False,
  )
  if created:
    cart_item.quantity = int(quantity)
    cart_item.save()
    return HttpResponse('Товар был добавлен в корзину')
  if not created:
    cart_item.quantity += int(quantity)
    cart_item.save()
    return HttpResponse('Количество товара в корзине было увеличено')


@csrf_exempt
def get_cart_items(request):
  sk = get_sk(request)
  cart_items = CartItem.objects.filter(
    sk=sk,
    ordered=False
  )
  serializer = CartItemSerializer(cart_items, many=True)

  total_order_price = 0
  for item in cart_items:
    total_order_price += int(item.total_price)

  response = {
    'cart_items':serializer.data, 
    'total_order_price':total_order_price
  }
  # print(serializer.data)
  return JsonResponse(response)


@csrf_exempt
def get_cart_items_amount(request):
  request.session.modified = True
  sk = get_sk(request)
  qs = CartItem.objects.filter(
    sk=sk,
    # user=get_user(request),
    ordered=False,
  )
  # print('get_cart_items_amount() cart_items:', qs)
  # print('get_cart_items_amount() sk:', sk)
  items_amount = 0
  for cart_item in qs:
    items_amount += cart_item.quantity
  # print(items_amount)
  return HttpResponse(items_amount)


@csrf_exempt
def remove_cart_item(request):
  id = request.POST['id']
  cart_item = CartItem.objects.get(id=id)
  cart_item.delete()
  return HttpResponse()



@csrf_exempt
def clear_cart(request):
  cart_items = CartItem.objects.filter(sk=get_sk(request))
  for cart_item in cart_items:
    cart_item.delete()
  return HttpResponse()


@csrf_exempt
def change_cart_item_amount(request):
  print(request.POST)
  id = request.POST['id']
  quantity = request.POST['quantity']
  # cart_item = CartItem.objects.get(sk=get_sk(request), id=id)
  cart_item = get_object_or_404(CartItem, sk=get_sk(request), id=id)
  cart_item.quantity = int(quantity)
  cart_item.save()
  total_order_price = 0
  for cart_item in CartItem.objects.all().filter(sk=get_sk(request)):
    total_order_price += cart_item.total_price
  total_price = int(quantity) * float(cart_item.item.price)
  response = {
    'total_price':total_price,
    'cart_item_id':cart_item.id,
    'total_order_price': total_order_price,
  }
  return JsonResponse(response)

