from cart.models import CartItem, FavourItem
from utils.utils import get_sk, get_user

def cart_content(request):
    cart_items = CartItem.objects.filter(
        sk=get_sk(request),
        ordered=False
    )
    favour_items = FavourItem.objects.filter(
        sk=get_sk(request),
    )
    cart_items_amount = 0
    for item in cart_items:
        cart_items_amount += item.quantity
    
    total_order_price = 0
    for item in cart_items:
        total_order_price += int(item.total_price)
    
    request.session['total_price'] = total_order_price
    return locals()