from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save, pre_save
from django.db import models
from functools import wraps
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext as _
from django.utils.text import slugify
from django.db.models import Sum
from django.shortcuts import reverse
from django.urls import reverse
from django_countries.fields import CountryField

User = settings.AUTH_USER_MODEL


class FavourItem(models.Model):
  item = models.ForeignKey('item.Item', on_delete=models.CASCADE, verbose_name='Улюблені товари', blank=True, null=True)
  sk   = models.CharField(max_length=120, blank=True, null=True)
  user = models.ForeignKey(User, verbose_name='Пользователь',on_delete=models.CASCADE, blank=True, null=True)
  def __str__(self):
    return f'{self.item.name},{self.user}, {self.sk}'
  class Meta:
    verbose_name=_("Улюблений товар"); verbose_name_plural=_("Улюблені товари"); 



class CartItem(models.Model):
  user           = models.ForeignKey(User, verbose_name='Пользователь',on_delete=models.CASCADE, blank=True, null=True)
  sk             = models.CharField(verbose_name='Ключ сессии',max_length=120, blank=True, null=True)
  item           = models.ForeignKey('item.Item', verbose_name='Товар',on_delete=models.CASCADE, blank=True, null=True)
  order          = models.ForeignKey('order.Order', verbose_name='Заказ',on_delete=models.CASCADE, blank=True, null=True)
  # size           = models.CharField(verbose_name='Размер',max_length=20, blank=True, null=True)
  quantity       = models.IntegerField(verbose_name='Количество',default=1)
  price_per_item = models.DecimalField(verbose_name='Цена за шт',max_digits=10, decimal_places=2, blank=True, null=True)
  total_price    = models.DecimalField(verbose_name='Суммарная стоимость',max_digits=10, decimal_places=2, blank=True, null=True)
  ordered        = models.BooleanField(verbose_name='Заказан?',default=False, blank=True, null=True)
  created        = models.DateTimeField(verbose_name='Дата создания',auto_now_add=True, auto_now=False, blank=True, null=True)
  updated        = models.DateTimeField(verbose_name='Дата обновления',auto_now_add=False, auto_now=True, blank=True, null=True)
  # objects        = OrderItemManager()
  def __str__(self):
    return f'{self.item.name}, {self.quantity}штука, {self.total_price}'
  def save(self, *args, **kwargs):
    price_per_item = self.item.price
    self.price_per_item = price_per_item
    self.total_price = self.quantity * price_per_item
    super().save(*args, **kwargs)
  def get_total_price(self, *args, **kwargs):
    return self.quantity * self.item.price
    # super().save(*args, **kwargs)
  class Meta: verbose_name = _('Товар в корзині'); verbose_name_plural = _('Товари в корзинах'); 
