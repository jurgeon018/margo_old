from django.contrib import admin
from .models import *
from django.forms import widgets 






class CartItemAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CartItem._meta.fields]
    exclude = ['sk', 'user']
    class Meta:
        model = CartItem
admin.site.register(CartItem, CartItemAdmin)


class CartItemInline(admin.TabularInline):
    model = CartItem
    extra = 0
    # exclude = ['sk']
    readonly_fields = ['sk','user', 'item', 'quantity',]


class FavourItemAdmin (admin.ModelAdmin):
    list_display = [field.name for field in FavourItem._meta.fields]
    exclude = ['sk', 'user']
    class Meta:
        model = FavourItem
admin.site.register(FavourItem, FavourItemAdmin)


class CartItemInline(admin.TabularInline):
    model = CartItem
    extra = 0
    # exclude = ['sk']
    readonly_fields = ['sk','user', 'item', 'quantity',]


