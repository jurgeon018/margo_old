var params = {};
function select__item(e){
    all     = $('.select_value_name')//document.querySelectorAll('.select_value_name')
    all     = [...all]// or Array.from(all) // https://stackoverflow.com/questions/32765157/filter-or-map-nodelists-in-es6
    size    = all.filter((item) => item.dataset.type == 'size')
    form    = all.filter((item) => item.dataset.type == 'form')
    sorting = all.filter((item) => item.dataset.type == 'sorting')
    size    = size[0].dataset.id
    form    = form[0].dataset.id
    sorting = sorting[0].dataset.id
    args    = {
      'size':size,
      'form':form,
      'sorting':sorting,
    }
    params['args'] = args
    initialize(params)
}

function select_page(e){
  e.preventDefault();
  params['link'] = e.target.dataset.href.replace('?','')
  initialize(params)
}


$('document').ready(function(){
  initialize(params);
  $('.select__item').click(select__item)
  $('.link').click(select_page)
})