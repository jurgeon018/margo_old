$(function() {

$('.header__main_item').on('click',function(event){
  event.preventDefault();
  window.location = $(this).children('a').attr('href');
});

if($('input[type="tel"]').length>0){
  $('input[type="tel"]').mask("+380(99)999-99-99");
}

/*auth start*/
$('.sign-up').on('click',function(event){
  event.preventDefault();
  $('.auth_form').removeClass('form-active');
  $('.register-form').addClass('form-active');
});
$('.login-form-btn').on('click',function(event){
  event.preventDefault();
  $('.auth_form').removeClass('form-active');
  $('.login-form').addClass('form-active');
});
$('.forgot-btn').on('click',function(event){
  event.preventDefault();
  $('.auth_form').removeClass('form-active');
  $('.forgot-form').addClass('form-active');
});
/*auth end*/

/*product cart start*/

  if(document.getElementsByClassName('Gallery').length > 0){
    var galleryTop = new Swiper('.Gallery', {
      direction: 'vertical',
      spaceBetween: 10,
      slideToClickedSlide: true,
      navigation: {
          nextEl: '.product-card__pagination_top',
          prevEl: '.product-card__pagination_buttoms',
        }
    });
    var galleryThumbs = new Swiper('.Thumbs', {
      direction: 'vertical',
      spaceBetween: 10,
      loop:true,
      centeredSlides: true,
      slidesPerView: 5,
      touchRatio: 0.2,
      slideToClickedSlide: true,
      navigation: {
          nextEl: '.product-card__pagination_top',
          prevEl: '.product-card__pagination_buttoms',
        },
      thumbs: {
          swiper: galleryTop,
        }
    }
    );
  }

  $('.product-cart__size_item').on('click',function() {
    $('.product-cart__size_item').removeClass('product-cart__size_item-active');
    $(this).toggleClass('product-cart__size_item-active');
    if ( $('.product-cart__size-input').length>0) {
          $('.product-cart__size-input').val($(this).data('size'));
    }


  })
  //select product cart size start
  $('.select_size').on('click', function(event) {
     console.log("crash");
    var name_item,id_item,current_value,temp_item = {};
    name_item = $(this).text();
    id_item = $(this).data('size');
    console.log(id_item);
    current_value = $(this).parent(".select__items").siblings(".select_value").find('.select_value_name');
    temp_item.value = current_value.text();
    temp_item.id = current_value.data('size');
    console.log(temp_item.id);
    current_value.text(name_item);
    current_value.data('size',id_item);
    $(this).text(temp_item.value)
    $(this).data('size',temp_item.id)
    if ( $('.product-cart__size-input').length>0) {
          $('.product-cart__size-input').val(id_item);
    }
  })
//select product cart size end
/*product cart end*/

// CARD LIKE START

$(".like__box").on('click', function(e) {
    $(this).toggleClass('like__box-active');
   });
// CARD LIKE END


  $('.product__group_plus').on('click',function(){
    var element = $(this);
    update_count(element);
    update_prise(element);

  });
  $('.product__group_minus').on('click',function(){
    var element = $(this);
    console.log(element);
    update_count(element);
    update_prise(element);

  });

// product__group_plus();
// function product__group_plus(){
//   $('.product__group_plus').on('click',function(){
//     var element = $(this);
//     update_count(element);
//     update_prise(element);

//   });
// }

  $('.product__quantity_count').on('keyup',function(){
    update_prise($(this));
  })

  function update_count(element){
    var product_count_value = $(element).siblings('.product__quantity_count').val();
    var product_count = $(element).siblings('.product__quantity_count');

    if($(element).hasClass('product__group_plus')) {
       product_count.val(parseInt(product_count_value)+1);
     } else {
        if (product_count_value>0) {
          product_count.val(parseInt(product_count_value)-1);
        }
     }
  }
  function update_prise(element){
    var product_count_value;
    var product_count = element.parent('.product__quantity');

    var product_prise = parseFloat(element.parent('.product__quantity').data('prise'));

    if ($(element).hasClass('product__quantity_count') ) {
      product_count_value = $(element).val();
    }else {
      product_count_value = element.siblings('.product__quantity_count').val();
    }
    var all_product_prise =  product_count.siblings('.product__price_box').find('.product__price').text(parseFloat(product_prise*product_count_value).toFixed(2));
  }

  if(document.getElementsByClassName('like-list-form').length > 0){
    var like_modaal = $(".like-list-form").modaal({
        content_source: '#like_list',
        hide_close: true,
        // background: #131313,
        background_scroll: false
    });
    $('.basket_close').on('click', function() {
        like_modaal.modaal('close');
          // basket_modaal.modaal('close');
    });
  }

  if(document.getElementsByClassName('basket-form').length > 0){
    var basket_modaal = $(".basket-form").modaal({
        content_source: '#basket',
        hide_close: true,
        // background: #131313,
        background_scroll: false
    });
    $('.basket_close').on('click', function() {
        basket_modaal.modaal('close');
    });
  }

  // if(document.getElementsByClassName('header__login_login').length > 0){
  //   var фгер_modaal = $(".header__login_login").modaal({
  //       content_source: '#auth',
  //       custom_class:'auth_wrap',
  //       hide_close: true,
  //       width:500,
  //       // background: #131313,
  //       background_scroll: false
  //   });
  // }


  // $('.basket_close').on('click', function() {
  //     like_modaal.modaal('close');
  // });
  // $('.basket_close,.continue_shopping').on('click', function() {
  //     basket_modaal.modaal('close');
  // });

  var $slickElement = $('.slider__wrap');

  if($slickElement.length>0){
    $slickElement.slick({
        dots: true,
        infinite: true,
        arrows: false,
        speed: 300,
        slidesToShow: 1,
        customPaging: function(slider, i) {
            return '<button class="tab">' + '<div class="title">' + $(slider.$slides[i]).attr('title') + '</div>' + '</button>';
        },
        adaptiveHeight: true
    });
    $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
        var i = (currentSlide ? currentSlide : 0) + 1;
        $('.slider__pagination_current').text(i > 10 ? i : '0' + i);
        $('.slider__pagination_all').text(slick.slideCount > 10 ? slick.slideCount : '0' + slick.slideCount);
    });
  }
    var $partner__wrap = $('.partner__wrap');
    if($partner__wrap.length>0){
      $partner__wrap.slick({
          dots: true,
          infinite: true,
          arrows: false,
          speed: 300,
          slidesToShow: 5,
          slidesToScroll: 2,
          centerMode: true,
          dotsClass: "partner__dots",
          variableWidth: true,
          responsive: [{
              breakpoint: 992,
              settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3
              }
          }]
      });
    }

    $(".partner__prew_item").on('click', function() {
        $('.partner__wrap').slick('slickPrev');
    });
    $(".partner__next_item").on('click', function() {
        $('.partner__wrap').slick('slickNext');
    });


    $('.lang__arrow').on('click', function(event) {
        event.preventDefault();
        $(this).toggleClass("lang__arrow-active");
        $(this).siblings('.lang__items').toggleClass("lang__items-active");
    });

    $(".js-hamburger").on('click', function(e) {
        $('.js-hamburger').toggleClass('is-active');
        $('.header__main').toggleClass('header__main-active');
        $('.main-menu__backgaunt').toggleClass('main-menu__backgaunt-active');
    });

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object
        console.log(files);
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var file_logo = document.getElementById('file-logo');
                    var img = '<img class="thumb" src="' + e.target.result + '" title="' + escape(theFile.name) + '"/><div id="delete__file" class="delete__file"></div>';
                    file_logo.innerHTML = img;
                    file_logo.classList.add("form__input__img-send");
                    delete__file();
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    function delete__file() {
        $("#delete__file").on('click', function(event) {
            var file_logo = document.getElementById('file-logo');
            var img = '<svg width="21" height="17" viewBox="0 0 21 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20.6379 0.172363H0.362069C0.162207 0.172363 0 0.334208 0 0.534432V16.4655C0 16.6657 0.162207 16.8275 0.362069 16.8275H20.6379C20.8378 16.8275 21 16.6657 21 16.4655V0.534432C21 0.334208 20.8378 0.172363 20.6379 0.172363ZM20.2759 16.1034H0.724138V0.896501H20.2759V16.1034Z" fill="#969696" /><path d="M5.79322 8.18775C6.90513 8.18775 7.80958 7.28331 7.80958 6.17175C7.80958 5.05948 6.90513 4.15503 5.79322 4.15503C4.6813 4.15503 3.77686 5.05948 3.77686 6.17139C3.77686 7.28331 4.6813 8.18775 5.79322 8.18775ZM5.79322 4.87917C6.50577 4.87917 7.08544 5.4592 7.08544 6.17139C7.08544 6.88358 6.50577 7.46362 5.79322 7.46362C5.08067 7.46362 4.50099 6.88394 4.50099 6.17175C4.50099 5.45956 5.08067 4.87917 5.79322 4.87917Z" fill="#969696" /><path d="M2.53462 14.6552C2.61935 14.6552 2.7048 14.6255 2.77359 14.5651L8.68002 9.36502L12.4101 13.0947C12.5516 13.2363 12.7805 13.2363 12.922 13.0947C13.0636 12.9531 13.0636 12.7243 12.922 12.5827L11.1816 10.8423L14.5057 7.20202L18.583 10.9397C18.7303 11.0747 18.9595 11.0646 19.0946 10.9172C19.2296 10.7698 19.2198 10.5407 19.0721 10.4056L14.7273 6.42285C14.6563 6.35804 14.5618 6.32581 14.4666 6.32798C14.3707 6.33233 14.2801 6.37469 14.2153 6.44566L10.6692 10.3296L8.95193 8.61228C8.81652 8.47722 8.6 8.47035 8.45662 8.59635L2.2953 14.0212C2.14504 14.1534 2.13055 14.3822 2.26271 14.5325C2.3344 14.6139 2.43433 14.6552 2.53462 14.6552Z" fill="#969696" /></svg>';
            file_logo.innerHTML = img;
            file_logo.classList.remove("form__input__img-send");
            return false;
        });
    }

    if (document.getElementById('photo')) {
        document.getElementById('photo').addEventListener('change', handleFileSelect, false);
    }

    $('.form__input__img').on('click', function() {
        $('.form__items').removeClass('form__items-active');
        $("#photo").trigger("click");
    });

    $(".form__input_name,.form__input_control").on('click', function() {
        var items_list = $(this).siblings(".form__items");
        if ($(items_list).hasClass('form__items-active')) {
            $(items_list).toggleClass('form__items-active');
        } else {
            $('.form__items').removeClass('form__items-active');
            $(items_list).toggleClass('form__items-active');
        }
    })

    $(".form__items_item").on('click', function() {
        var id_items, input_value;
        var items_list = $(this);
        if (!$(items_list).hasClass('form__items_item-active')) {
            id_items = $(items_list).data('id');
            input_value = $(items_list).parent(".form__items").siblings(".input_value");
            $('.form__items_item').removeClass('form__items_item-active');
            $(items_list).toggleClass('form__items_item-active');
            input_value[0].value = id_items;
        }
    })
    /*
sellect
    */
    $('.select_value').on('click', function(event) {
console.log($(this).hasClass('select_value-active'));
      if($(this).hasClass('select_value-active')){
        $(this).siblings(".select__items").toggleClass("select__items-active");
        $(this).toggleClass("select_value-active");
      }else{
        $('.select_value').removeClass("select_value-active");
        $('.select__items').removeClass("select__items-active");
        $(this).siblings(".select__items").toggleClass("select__items-active");
        $(this).toggleClass("select_value-active");
      }


    });


    $('.select__item').on('click', function(event) {
      var name_item,id_item,current_value,temp_item = {};
      name_item = $(this).text();
      id_item = $(this).data('id');
      current_value = $(this).parent(".select__items").siblings(".select_value").find('.select_value_name');
      temp_item.value = current_value.text();
      temp_item.id = current_value.data('id');
      current_value.text(name_item);
      current_value.data('id',id_item);
      current_value[0].dataset.id = id_item /////!!!!!!
      $(this).text(temp_item.value)
      $(this).data('id',  temp_item.id)
      if ( $('.product-cart__size-input').length>0) {
            $('.product-cart__size-input').val(id_item);
      }

    });



/*
select end
*/
/*order basket start*/
// $('.make_order_form').submit(function () {
//   console.log("crash");
//     var form = $(this);
//     var field = [];
//     form.find('input[data-validate]').each(function () {
//       field.push('input[data-validate]');
//       var value = $(this).val(),

//           line = $(this).siblings('.form-group_error');
// console.log($(this));
//       for(var i=0;i<field.length;i++) {
//     console.log(value);
//         if( !value ) {

//           line.addClass('form-group_error-active');
//           setTimeout(function() {
//             line.removeClass('form-group_error-active')
//           }.bind(this),2000);
//           event.preventDefault();
//         }
//       }
//     });
//   });
/*order basket end*/
/*
filter start
*/
  $('.catalog__filter_img').on('click',function(){
    $(this).toggleClass('catalog__filter_img-active');
    $(this).siblings('.catalog__filter_box').toggleClass('catalog__filter_box-active');
  })
/*
filter end
*/
$('.select__item').removeClass('select__item_mobil');
    if (document.documentElement.clientWidth < 768) {
        // $(".partner__dots").prepend('<div class="partner__prew"> <div class="partner__prew_item"></div> </div>');
        // $(".partner__dots").append('<div class="partner__next"> <div class="partner__next_item"></div> </div>');


        var steps = $("#constructor__form fieldset");
        // console.log(steps);
        var count = steps.length;
        steps.each(function(i) {
            var page_step = i + 1;
            $(this).wrap("<div class='fieldset__box' id='step" + i + "'></div>");
            $(this).prepend("<div class='constructor__pagination'><div class='constructor__pagination_current'>" + 0 + page_step + "</div><div class='constructor__pagination_centr'>/</div><div class='constructor__pagination_all'>" + 0 + count + "</div></div>");
            $(this).append("<div id='step" + i + "commands' class='constructor__pagination_box'></div>");
            if (i == 0) {

                createNextButton(i); // to do
                // selectStep(i); // to do
            } else if (i == count - 1) {
                $("#step" + i).hide();
                createPrevButton(i); // to do

            } else {
                $("#step" + i).hide();
                createPrevButton(i); // to do
                createNextButton(i); // to do
            }
        })

$('.select__items-line').addClass('select__item_mobil');
$('.select_value-line').addClass('select_value-line_mobil');

    }




    function createPrevButton(i) {
        var stepName = "step" + i;
        $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='prev constructor__pagination_prev'>Назад</a>");
        $("#" + stepName + "Prev").bind("click", function(e) {
            $("#" + stepName).hide();
            $("#step" + (i - 1)).show();
            // selectStep(i - 1);
        });
    }

    function createNextButton(i) {
        var stepName = "step" + i;

        $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Next' class='constructor__pagination_next btn'>Продовжити  </a>");

        $("#" + stepName + "Next").bind("click", function(e) {
            if (i == 0) {
                if ($(' input[type=file]').val() != "") {

                }
            }

            $("#" + stepName).hide();

            $("#step" + (i + 1)).show();
            // selectStep(i + 1);
        });

    }




  /*lazy loader start*/

  var imageObserver = new IntersectionObserver(function (entries, imgObserver) {
      entries.forEach(function (entry) {
          if (entry.isIntersecting) {
              var lazyImage = entry.target;
              // console.log("lazy loading ", lazyImage);
              lazyImage.src = lazyImage.dataset.src;
              lazyImage.classList.remove("lzy_img");
              imgObserver.unobserve(lazyImage);
          }
      });
  });
  var arr = document.querySelectorAll('.lazyload');
  arr.forEach(function (v) {
      imageObserver.observe(v);
  });

  /*lazy loader end*/


  // var bLazy = new Blazy({
  //         // container: '.lazyload' // Default is window
  //     });





    var $bird = document.getElementById('bird');
    if(typeof($bird) != 'undefined' && $bird != null){
      sprite({
        x:0,
        y:0,
        width:300,
        height:300,
        one_photo:true,
        src:'/static/img/bird.png',
        background_src:'/static/img/bird_c.png',
        canvas_id:'bird',
        step_photo:304
      });
      $bird.onmouseover = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 49,
            curentFrame:0,
            src:'/static/img/bird.png',
            background_src:'/static/img/bird_c.png',
            canvas_id:'bird',
            step_photo:304
          });
      };
      $bird.onmouseout = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 1,
            curentFrame:50,
            src:'/static/img/bird.png',
            background_src:'/static/img/bird_c.png',
            canvas_id:'bird',
            step_photo:304
          });
      };
    }

    var flower = document.getElementById('flower');
    if(typeof(flower) != 'undefined' && flower != null){
      sprite({
        x:0,
        y:0,
        width:300,
        height:300,
        one_photo:true,
        src:'/static/img/flower.png',
        background_src:'/static/img/flower_bg.png',
        canvas_id:'flower',
        step_photo:300
      });
      flower.onmouseover = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 45,
            curentFrame:0,
            src:'/static/img/flower.png',
            background_src:'/static/img/flower_bg.png',
            canvas_id:'flower',
            step_photo:300
          });
      };
      flower.onmouseout = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 1,
            curentFrame:46,
            src:'/static/img/flower.png',
            background_src:'/static/img/flower_bg.png',
            canvas_id:'flower',
            step_photo:300
          });
      };
    }

    var ship = document.getElementById('ship');
    if(typeof(ship) != 'undefined' && ship != null){
      sprite({
        x:0,
        y:0,
        width:300,
        height:300,
        one_photo:true,
        src:'/static/img/ship.png',
        background_src:'/static/img/ship_bg.png',
        canvas_id:'ship',
        step_photo:300
      });
      ship.onmouseover = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 49,
            curentFrame:0,
            src:'/static/img/ship.png',
            background_src:'/static/img/ship_bg.png',
            canvas_id:'ship',
            step_photo:300
          });
      };
      ship.onmouseout = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 1,
            curentFrame:49,
            src:'/static/img/ship.png',
            background_src:'/static/img/ship_bg.png',
            canvas_id:'ship',
            step_photo:300
          });
      };
    }

    var swan = document.getElementById('swan');
    if(typeof(swan) != 'undefined' && swan != null){
      sprite({
        x:0,
        y:0,
        width:300,
        height:300,
        one_photo:true,
        src:'/static/img/swan.png',
        background_src:'/static/img/swan_bg.png',
        canvas_id:'swan',
        step_photo:300
      });
      swan.onmouseover = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 43,
            curentFrame:0,
            src:'/static/img/swan.png',
            background_src:'/static/img/swan_bg.png',
            canvas_id:'swan',
            step_photo:300
          });
      };
      swan.onmouseout = function(){
          sprite({
            x:0,
            y:0,
            width:300,
            height:300,
            frameCount: 1,
            curentFrame:43,
            src:'/static/img/swan.png',
            background_src:'/static/img/swan_bg.png',
            canvas_id:'swan',
            step_photo:300
          });
      };
    }


    function sprite(options) {

      var spriteOptions = null,
          curentFrame = null,
          charset = null,
          ctx = null,
          canvas = null,
          background = null;
      spriteOptions = {};
      spriteOptions.one_photo = options.one_photo || false;
      spriteOptions.curentFrameflag = options.curentFrame;
      curentFrame = options.curentFrame;
      spriteOptions.x = options.x;
      spriteOptions.y = options.y;
      spriteOptions.width = options.width;
      spriteOptions.height = options.height;
      spriteOptions.frameCount = options.frameCount;
      spriteOptions.img_src = options.src;
      spriteOptions.background_src = options.background_src;
      spriteOptions.canvas_id = options.canvas_id;
      spriteOptions.step_photo = options.step_photo;

      srcX = 0;
      srcY = 0;

      // anime images
      charset = new Image();
      charset.src = spriteOptions.img_src;

      // background images
      background = new Image();
      background.src = spriteOptions.background_src;

      // canvas poligon
      canvas = document.getElementById(spriteOptions.canvas_id);
      canvas.width = options.width;
      canvas.height = options.height;
      ctx = canvas.getContext("2d");

      // drawing photo
      spriteOptions.drawImage = function () {
          spriteOptions.timerId = setInterval(function () {
              if (spriteOptions.curentFrameflag < spriteOptions.frameCount) {
                  if (curentFrame <= spriteOptions.frameCount) {
                      spriteOptions.updateFrame();
                      ctx.drawImage(background, 0, 0, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
                      ctx.drawImage(charset, srcX, srcY, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
                  }
              } else {
                  if (curentFrame >= spriteOptions.frameCount) {
                      spriteOptions.updateFrame();
                      ctx.drawImage(background, 0, 0, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
                      ctx.drawImage(charset, srcX, srcY, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
                  }
              }
          }, 15);
      };
      //update drawing photo
      spriteOptions.updateFrame = function () {
          if (spriteOptions.curentFrameflag < spriteOptions.frameCount) {
              curentFrame = ++curentFrame;
          } else {
              curentFrame = --curentFrame;
          }
          srcX = curentFrame * spriteOptions.step_photo;
          srcY = 0;
          ctx.clearRect(spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
      };
      charset.addEventListener("load", function () {
          spriteOptions.drawImage();
          // drawing one phpto
          if (spriteOptions.one_photo === true) {
              ctx.drawImage(charset, srcX, srcY, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
              ctx.drawImage(background, 0, 0, spriteOptions.width, spriteOptions.height, spriteOptions.x, spriteOptions.y, spriteOptions.width, spriteOptions.height);
          }
      });
  }

  $(".tabs-list li a").click(function(e){
       e.preventDefault();
    });

    $(".header__main_item-admin").click(function(){
       var tabid = $(this).attr("href");
       $(".tabs-list li,.tabs div.tab").removeClass("active");   // removing active class from tab
       $(".tab").hide();   // hiding open tab
       $(tabid).show();    // show tab
       $(this).addClass("active"); //  adding active class to clicked tab

    });

 })
