from PIL import Image
# im = Image.open('star.png').convert("RGBA")
# logo = Image.open('clouds.png').convert("RGBA")
# box = (230,135,235,235)
# im.crop(box)
# logo = logo.resize((box[2] - box[0], box[3] - box[1]))
# im.paste(logo,box)
# im.save('new.png')


import glob

foreground = Image.open('clouds.png').convert('RGBA')
background = Image.open('star.png')

x = int((background.size[0] / 0.25) - (foreground.size[0] / 0.25))
y = int((background.size[1] / 0.25) - (foreground.size[1] / 0.25))

background = background.convert('RGBA')
background.paste(foreground, (x, y), mask = foreground)
background.save('new.png')


