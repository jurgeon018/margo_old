from PIL import Image, ImageDraw

image = Image.open('default.png')
width, height = image.size
center = int(0.5 * width), int(0.5 * height)
colour = 0, 255, 0, 255
ImageDraw.floodfill(image, xy=center, value=colour)
image.save('star.png')