from PIL import Image

img = Image.open('star.png')
img = img.resize((400, 400), Image.ANTIALIAS)
img.save('new.png')


def paste_logo_on_template(napkin):
  now           = time()
  logo_path     = napkin.logo.path
  template_path = napkin.template.image.path
  
  template      = Image.open(template_path).convert('RGBA')
  logo          = Image.open(logo_path).convert('RGBA')
  logo          = resize_logo(logo)
  template.paste(logo, (300, 200), logo)
  return create_final_image(template, napkin)


def resize_logo(logo):
  baseheight = 300
  hpercent   = (baseheight / float(logo.size[1]))
  wsize      = int((float(logo.size[0]) * float(hpercent)))
  logo        = logo.resize((wsize, baseheight), Image.ANTIALIAS)
  return logo